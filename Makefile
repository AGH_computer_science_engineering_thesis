PROJ_DIR := ./

def : design.bin

include Makefile.util

IVFLAGS += -Iinclude/

TESTS := $(notdir \
	$(shell find $(PROJ_DIR)/tests -maxdepth 1 -mindepth 1 -type d))

TEST_TARGETS := $(addprefix test_,$(TESTS))

EXAMPLES := $(notdir \
	$(shell find $(PROJ_DIR)/examples -maxdepth 1 -mindepth 1 -type d))

EXAMPLE_SIM_TARGETS := $(addprefix simulate_,$(EXAMPLES))
EXAMPLE_BUILD_TARGETS := $(addprefix build_,$(EXAMPLES))
EXAMPLE_PROG_TARGETS := $(addprefix prog_,$(EXAMPLES))

design.v : design/rom.mem design/*.v design/font.mem
	$(IV) $(IVFLAGS) -E $(filter %.v,$^) \
		-DROM_WORDS_COUNT=$(call FILE_LINES,$<) -o $@

# Yosys synthesis (generates design.json), NextPNR routing (generates
# design.asc), bitstream generation (generates design.bin) and programming
# were moved to Makefile.util, so that they can be reused in examples.

timing.rpt : design.asc
	$(ICETIME) -d hx8k -mtr $@ $<


CALL_TESTS = \
	cd tests/; \
	for TEST in $(1); do \
		echo "** $$TEST "; \
		if ! $(MAKE) -C $$TEST $(2) 3>&1 1>/dev/null 2>&3; then \
			FAIL=true; \
		fi; \
	done; \
	[ "$$FAIL" != true ]

test :
	$(call CALL_TESTS,*)

# Will skip VGA tests, because they take loooong time
quicktest :
	$(call CALL_TESTS,*,QUICK_TEST=1)

stack_machine_test :
	$(call CALL_TESTS,stack_machine_*)

stack_machine_quicktest :
	$(call CALL_TESTS,stack_machine_*,QUICK_TEST=1)

wasm_compile_test :
	$(call CALL_TESTS,wasm_compile_*)

$(TEST_TARGETS) : test_% :
	$(MAKE) -C tests/$*

$(EXAMPLE_SIM_TARGETS) : simulate_% :
	$(MAKE) -C examples/$*

$(EXAMPLE_BUILD_TARGETS) : build_% :
	$(MAKE) -C examples/$* design.bin

$(EXAMPLE_PROG_TARGETS) : prog_% :
	$(MAKE) -C examples/$* prog


tools : $(TOOLS_TARGETS)

clean :
	for TEST in tests/*/; do $(MAKE) -C $$TEST clean >/dev/null; done
	for EXAMP in examples/*/; do $(MAKE) -C $$EXAMP clean > /dev/null; done
	rm $(call FIND_GENERATED_FILES,design/) 2>/dev/null || true
	$(MAKE) -C tools/ clean >/dev/null
	rm $(addprefix design.,v json asc bin) timing.rpt \
		$(addsuffix .log,yosys pnr) 2>/dev/null || true

.PHONY : def tools test quicktest stack_machine_test stack_machine_quicktest \
	wasm_compile_test $(TEST_TARGETS) $(TOOLS_TARGETS) tools \
	$(EXAMPLE_SIM_TARGETS) $(EXAMPLE_BUILD_TARGETS) $(EXAMPLE_PROG_TARGETS)
