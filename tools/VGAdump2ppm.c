#include <stdio.h>
#include <stdint.h>

int main(int argc, char **argv)
{
	uint8_t color_value = 0;
	uint8_t bits_read = 0;
	uint8_t channels_read = 0;
	int pixels_processed = 0;
	int input;

	/* http://netpbm.sourceforge.net/doc/ppm.html */
	puts("P3");
	puts("640 480");
	puts("7");

	while (1) {
		input = getchar();

		if (input == EOF)
			break;

		if (input != '1' && input != '0')
			continue;

		if (input == '1')
			color_value |= 1 << (2 - bits_read);

		if (++bits_read == 3) {
			printf(" %d", color_value);

			color_value = 0;
			bits_read = 0;

			if (++channels_read == 3) {
				channels_read = 0;

				putchar(++pixels_processed % 8 == 0 ?
					'\n' : ' ');
			}
		}
	}

	return 0;
}
