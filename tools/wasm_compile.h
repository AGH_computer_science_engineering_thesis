#ifndef WASM_COMPILE_H
#define WASM_COMPILE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>

#define STACK_FRAME_BACKUP_ADDR 0x0FFFFC
#define STACK_TOP_ADDR          0x0FFFFC
#define CODE_BOTTOM_ADDR        0x000000
#define CODE_TOP_ADDR           0x000400
#define MEMORY_BOTTOM_ADDR      0x000400

/* Error messages */
#define MSG_SIZE_OVERFLOW "Number overflows size_t\n"
#define MSG_EOF           "Unexpected end of bytes\n"
#define MSG_BAD_NUM_ENC   "Improper number encoding\n"
#define MSG_BAD_NUM       "Couldn't parse number\n"
#define MSG_BAD_SIZE      "Couldn't compute size to allocate\n"
#define MSG_ALLOC_FAIL(n) "Failed to allocate %lu bytes\n", (unsigned long) (n)
#define MSG_BAD(nam, val) "Found 0x%02hhx instead of %s\n", (char) (val), nam
#define MSG_BYTES_REMAIN  "Didn't use up all bytes when parsing\n"
#define MSG_SEEK_FAIL     "Couldn't navigate the file\n"
#define MSG_BAD_IDX(nam)  "Got %s out of range\n", nam

#define PRERR(...) fprintf(stderr, __VA_ARGS__)

#define POW(n) (((int64_t) 1) << (n))

struct resulttype {
	uint32_t count;
	char *types;
};

struct functype {
	struct resulttype args, results;
};

struct function {
	struct functype *type;

	uint32_t locals_count;
	char *locals;

	struct instruction *translated_body;
	struct target *targets;
	uint32_t start_addr;
};

struct export {
	char *name;
	char desc;
	uint32_t idx;
};

struct module {
	uint32_t functypes_count;
	struct functype *functypes;
	uint32_t functions_count;
	struct function *functions;
	enum {
		MEM_NONE = 0,
		MEM_MIN,
		MEM_MIN_MAX
	} memory_type;
	uint32_t mem_min, mem_max;
	uint32_t exports_count;
	struct export *exports;
	struct target *targets;
	struct instruction *startup;
};

struct translated_word {
	uint16_t contents;
	struct instruction *instr;
};

int leb_32(FILE *handle, uint32_t *result, bool with_sign);

inline static int leb_u32(FILE *handle, uint32_t *result)
{
	return leb_32(handle, result, false);
}

inline static int leb_s32(FILE *handle, int32_t *result)
{
	return leb_32(handle, (uint32_t *) result, true);
}

void free_expr(struct instruction *expr);

void free_targets(struct target *top);

void free_module(struct module *module);

struct module *parse_module(FILE *handle);

int translate(FILE *handle, struct function *function, struct module *module);

int assemble(uint32_t memory_size, struct translated_word memory[memory_size],
	     struct module *module);

#endif  /* WASM_COMPILE_H */
