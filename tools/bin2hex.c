#include <stdio.h>
#include <stdint.h>

int main(int argc, char **argv)
{
	int input;

	while (1) {
		input = getchar();

		if (input == EOF)
			break;

		printf("%02x", input);

		input = getchar();

		/* If we have odd number of bytes, pad with a NULL-byte. */
		if (input == EOF)
			input = 0;

		printf(" %02x\n", input);
	}

	return 0;
}
