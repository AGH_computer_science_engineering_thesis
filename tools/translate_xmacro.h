/* X-macro-like definition of translation routines for each webasm opcode */

/*  wasm_opcode_______**translation_routine**argument_types**result_type*/
TS (WASM_I32_ADD,      add,                 i32_i32,        i32)
TS (WASM_I32_SUB,      sub,                 i32_i32,        i32)
TS (WASM_I32_DIV_U,    div,                 i32_i32,        i32)
TS (WASM_I32_REM_U,    rem,                 i32_i32,        i32)
TS (WASM_I32_MUL,      mul,                 i32_i32,        i32)
TS (WASM_I32_EQ,       eq,                  i32_i32,        i32)
TS (WASM_I32_LT_S,     lt,                  i32_i32,        i32)
TS (WASM_I32_LT_U,     ult,                 i32_i32,        i32)
TS (WASM_I32_GT_S,     gt,                  i32_i32,        i32)
TS (WASM_I32_GT_U,     ugt,                 i32_i32,        i32)

TLS(WASM_I32_LOAD,     load_p,              i32,            i32)
TLS(WASM_I32_LOAD8_S,  loadbsx_p,           i32,            i32)
TLS(WASM_I32_LOAD8_U,  loadbzx_p,           i32,            i32)
TLS(WASM_I32_LOAD16_S, loadwsx_p,           i32,            i32)
TLS(WASM_I32_LOAD16_U, loadwzx_p,           i32,            i32)
TLS(WASM_I32_STORE,    store_p,             i32_i32,        empty)
TLS(WASM_I32_STORE8,   storeb_p,            i32_i32,        empty)
TLS(WASM_I32_STORE16,  storew_p,            i32_i32,        empty)

/*
 * There are more checks to be performed in case of if and br_if, but we do them
 * another way and only check for the i32 condition value here.
 */
TC (WASM_BLOCK,        block,               custom,         custom)
TC (WASM_LOOP,         loop,                custom,         custom)
TC (WASM_IF,           if,                  i32,            custom)
TC (WASM_BR,           br,                  custom,         custom)
TC (WASM_BR_IF,        br_if,               i32,            custom)
TC (WASM_CALL,         call,                custom,         custom)
TC (WASM_LOCAL_GET,    local_get,           empty,          custom)
TC (WASM_LOCAL_SET,    local_set,           custom,         empty)
TC (WASM_I32_CONST,    const,               empty,          i32)
