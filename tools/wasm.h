#define SECTION_CUSTOM   0
#define SECTION_TYPE     1
#define SECTION_IMPORT   2
#define SECTION_FUNCTION 3
#define SECTION_TABLE    4
#define SECTION_MEMORY   5
#define SECTION_GLOBAL   6
#define SECTION_EXPORT   7
#define SECTION_START    8
#define SECTION_ELEMENT  9
#define SECTION_CODE     10
#define SECTION_DATA     11

#define VALTYPE_I32 0x7F
#define VALTYPE_I64 0x7E
#define VALTYPE_F32 0x7D
#define VALTYPE_F64 0x7C

#define EXPORT_FUNCIDX   0x00
#define EXPORT_TABLEIDX  0x01
#define EXPORT_MEMIDX    0x02
#define EXPORT_GLOBALIDX 0x03

/* WebAssembly opcodes */
#define WASM_BLOCK         0x02
#define WASM_LOOP          0x03
#define WASM_IF            0x04
#define WASM_ELSE          0x05
#define WASM_END           0x0B
#define WASM_BR            0x0C
#define WASM_BR_IF         0x0D
#define WASM_CALL          0x10

#define WASM_LOCAL_GET     0x20
#define WASM_LOCAL_SET     0x21

#define WASM_I32_LOAD      0x28
#define WASM_I32_LOAD8_S   0x2C
#define WASM_I32_LOAD8_U   0x2D
#define WASM_I32_LOAD16_S  0x2E
#define WASM_I32_LOAD16_U  0x2F
#define WASM_I32_STORE     0x36
#define WASM_I32_STORE8    0x3A
#define WASM_I32_STORE16   0x3B
#define WASM_I64_STORE32   0x3E

#define WASM_I32_CONST     0x41

#define WASM_I32_EQ        0x46
#define WASM_I32_LT_S      0x48
#define WASM_I32_LT_U      0x49
#define WASM_I32_GT_S      0x4A
#define WASM_I32_GT_U      0x4B
#define WASM_I32_LE_S      0x4C
#define WASM_I32_LE_U      0x4D
#define WASM_I32_GE_S      0x4E
#define WASM_I32_GE_U      0x4F

#define WASM_I32_ADD       0x6A
#define WASM_I32_SUB       0x6B
#define WASM_I32_MUL       0x6C
#define WASM_I32_DIV_U     0x6E
#define WASM_I32_REM_U     0x70

static inline int is_valid_valtype(char value_type)
{
	if (value_type == VALTYPE_I32)
		return 1;

	if (value_type == VALTYPE_I64 ||
	    value_type == VALTYPE_F32 ||
	    value_type == VALTYPE_F64)
		PRERR("Only type i32 is recognized for now");

	return -1;

	/* return */
	/* 	value_type == VALTYPE_I32 || */
	/* 	value_type == VALTYPE_I64 || */
	/* 	value_type == VALTYPE_F32 || */
	/* 	value_type == VALTYPE_F64; */
}

static inline int is_valid_exportdesc(char desc)
{
	return
		desc == EXPORT_FUNCIDX ||
		desc == EXPORT_TABLEIDX ||
		desc == EXPORT_MEMIDX ||
		desc == EXPORT_GLOBALIDX;
}
