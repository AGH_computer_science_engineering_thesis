#include "wasm_compile.h"
#include "stack_machine_instruction.h"

void print_instructions(uint32_t count,
			struct translated_word memory[count])
{
	uint32_t i;
	char binary[17];
	uint16_t bits;
	int j;

	binary[16] = '\0';

	for (i = 0; i < count; i++) {
		bits = memory[i].contents;
		j = 16;

		while (j--) {
			binary[j] = (bits & 1) ? '1' : '0';
			bits >>= 1;
		}

		printf(binary);

		if (memory[i].instr) {
			printf(" // 0x%03lx %s", (long) i,
			       memory[i].instr->name);
		}

		putchar('\n');
	}
}

int main(int argc, char **argv)
{
	FILE *handle = NULL;
	struct module *module = NULL;
	struct translated_word *translated_instructions = NULL;
	char retval = -1;

	if (argc < 2) {
		PRERR("Please provide name of the file to parse on the command line\n");
		goto fail;
	}

	handle = fopen(argv[1], "r");

	if (!handle) {
		PRERR("Couldn't open '%s'\n", argv[1]);
		goto fail;
	}

	module = parse_module(handle);

	if (!module)
		goto fail;

	translated_instructions = calloc(1, CODE_TOP_ADDR *
					 sizeof(struct translated_word));

	if (!translated_instructions) {
		PRERR(MSG_ALLOC_FAIL(CODE_TOP_ADDR));
		goto fail;
	}

	if (assemble(CODE_TOP_ADDR / 2, translated_instructions, module))
		goto fail;

	print_instructions(CODE_TOP_ADDR / 2, translated_instructions);

	retval = 0;

fail:
	if (handle)
		fclose(handle);

	free_module(module);

	free(translated_instructions);

	return retval;
}
