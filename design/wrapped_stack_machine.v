/*
 * This is a version of stack machine with 16-bit data ports
 * on *both* Wishbone interfaces (data interface is wrapped).
 * CLK_I and RST_I signals are shared between interfaces.
 * Two interfaces can, but don't have to, be made to access the same memory map.
 * Instructions interface never performs writes (its WE_O is hardwired to low).
 *
 * | *WISHBONE DATASHEET*                                                      |
 * |---------------------------------------------------------------------------|
 * | *Description*                   | *Specification*                         |
 * |---------------------------------+-----------------------------------------|
 * | General description             | stack machine core data interface       |
 * |---------------------------------+-----------------------------------------|
 * | Supported cycles                | MASTER, pipelined READ/WRITE            |
 * |---------------------------------+-----------------------------------------|
 * | Data port, size                 | 16-bit                                  |
 * | Data port, granularity          | 16-bit                                  |
 * | Data port, maximum operand size | 16-bit                                  |
 * | Data transfer ordering          | Big endian and/or little endian         |
 * | Data transfer ordering          | Undefined                               |
 * | Address port, size              | 20-bit                                  |
 * |---------------------------------+-----------------------------------------|
 * | Clock frequency constraints     | NONE                                    |
 * |---------------------------------+-----------------------------------------|
 * |                                 | *Signal name*    | *WISHBONE Equiv.*    |
 * |                                 |------------------+----------------------|
 * |                                 | D_ACK_I          | ACK_I                |
 * |                                 | D_ADR_O          | ADR_O()              |
 * | Supported signal list and cross | CLK_I            | CLK_I                |
 * |     reference to equivalent     | D_DAT_I          | DAT_I()              |
 * |     WISHBONE signals            | D_DAT_O          | DAT_O()              |
 * |                                 | D_SEL_O          | SEL_O                |
 * |                                 | D_STB_O          | STB_O                |
 * |                                 | D_CYC_O          | CYC_O                |
 * |                                 | D_WE_O           | WE_O                 |
 * |                                 | RST_I            | RST_I                |
 * |                                 | D_STALL_I        | STALL_I              |
 * |---------------------------------+-----------------------------------------|
 * | Special requirements            | NONE                                    |
 *
 *
 * | *WISHBONE DATASHEET*                                                      |
 * |---------------------------------------------------------------------------|
 * | *Description*                   | *Specification*                         |
 * |---------------------------------+-----------------------------------------|
 * | General description             | stack machine core instructions         |
 * |                                 |     interface                           |
 * |---------------------------------+-----------------------------------------|
 * | Supported cycles                | MASTER, pipelined READ                  |
 * |---------------------------------+-----------------------------------------|
 * | Data port, size                 | 16-bit                                  |
 * | Data port, granularity          | 16-bit                                  |
 * | Data port, maximum operand size | 16-bit                                  |
 * | Data transfer ordering          | Big endian and/or little endian         |
 * | Data transfer ordering          | Undefined                               |
 * | Address port, size              | 20-bit                                  |
 * |---------------------------------+-----------------------------------------|
 * | Clock frequency constraints     | NONE                                    |
 * |---------------------------------+-----------------------------------------|
 * |                                 | *Signal name*    | *WISHBONE Equiv.*    |
 * |                                 |------------------+----------------------|
 * |                                 | I_ACK_I          | ACK_I                |
 * |                                 | I_ADR_O          | ADR_O()              |
 * | Supported signal list and cross | CLK_I            | CLK_I                |
 * |     reference to equivalent     | I_DAT_I          | DAT_I()              |
 * |     WISHBONE signals            | I_DAT_O          | DAT_O()              |
 * |                                 | I_SEL_O          | SEL_O                |
 * |                                 | I_STB_O          | STB_O                |
 * |                                 | I_CYC_O          | CYC_O                |
 * |                                 | I_WE_O           | WE_O                 |
 * |                                 | RST_I            | RST_I                |
 * |                                 | I_STALL_I        | STALL_I              |
 * |---------------------------------+-----------------------------------------|
 * | Special requirements            | NONE                                    |
 */

`default_nettype none

module wrapped_stack_machine
  (
   /* Those 2 are supposed to be common for both wishbone interfaces */
   input wire 	      CLK_I,
   input wire 	      RST_I,

   /* Instruction reading interface */
   input wire 	      I_ACK_I,
   output wire [19:0] I_ADR_O,
   input wire [15:0]  I_DAT_I,
   output wire [15:0] I_DAT_O, /* Not used, interface read-only */
   output wire 	      I_STB_O,
   output wire 	      I_CYC_O,
   output wire 	      I_WE_O, /* Always 0, interface read-only */
   input wire 	      I_STALL_I,

   /* Data interface */
   input wire 	      D_ACK_I,
   output wire [19:0] D_ADR_O,
   input wire [15:0]  D_DAT_I,
   output wire [15:0] D_DAT_O,
   output wire 	      D_STB_O,
   output wire 	      D_CYC_O,
   output wire 	      D_WE_O,
   input wire 	      D_STALL_I,

   /* non-wishbone */
   output wire 	      finished
   );

   wire 	      D_RAW_ACK_I;
   wire [20:0] 	      D_RAW_ADR_O;
   wire [31:0] 	      D_RAW_DAT_I;
   wire [31:0] 	      D_RAW_DAT_O;
   wire [3:0] 	      D_RAW_SEL_O;
   wire 	      D_RAW_STB_O;
   wire 	      D_RAW_CYC_O;
   wire 	      D_RAW_WE_O;
   wire 	      D_RAW_STALL_I;

   stack_machine_new stack_machine
     (
      .CLK_I(CLK_I),
      .RST_I(RST_I),

      /* Instruction reading interface */
      .I_ACK_I(I_ACK_I),
      .I_ADR_O(I_ADR_O),
      .I_DAT_I(I_DAT_I),
      .I_DAT_O(I_DAT_O),
      .I_STB_O(I_STB_O),
      .I_CYC_O(I_CYC_O),
      .I_WE_O(I_WE_O),
      .I_STALL_I(I_STALL_I),

      /* Data interface */
      .D_ACK_I(D_RAW_ACK_I),
      .D_ADR_O(D_RAW_ADR_O),
      .D_DAT_I(D_RAW_DAT_I),
      .D_DAT_O(D_RAW_DAT_O),
      .D_SEL_O(D_RAW_SEL_O),
      .D_STB_O(D_RAW_STB_O),
      .D_CYC_O(D_RAW_CYC_O),
      .D_WE_O(D_RAW_WE_O),
      .D_STALL_I(D_RAW_STALL_I),

      .finished(finished)
      );

   interface_wrapper wrapper
     (
      .CLK_I(CLK_I),
      .RST_I(RST_I),

      .RAW_ACK_I(D_RAW_ACK_I),
      .RAW_ADR_O(D_RAW_ADR_O),
      .RAW_DAT_I(D_RAW_DAT_I),
      .RAW_DAT_O(D_RAW_DAT_O),
      .RAW_SEL_O(D_RAW_SEL_O),
      .RAW_STB_O(D_RAW_STB_O),
      .RAW_CYC_O(D_RAW_CYC_O),
      .RAW_WE_O(D_RAW_WE_O),
      .RAW_STALL_I(D_RAW_STALL_I),

      .WRAPPED_ACK_I(D_ACK_I),
      .WRAPPED_ADR_O(D_ADR_O),
      .WRAPPED_DAT_I(D_DAT_I),
      .WRAPPED_DAT_O(D_DAT_O),
      .WRAPPED_STB_O(D_STB_O),
      .WRAPPED_CYC_O(D_CYC_O),
      .WRAPPED_WE_O(D_WE_O),
      .WRAPPED_STALL_I(D_STALL_I)
      );
endmodule // wrapped_stack_machine
