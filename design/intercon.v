/* All this module does is combining slave_dispatcher with master_arbiter */
`default_nettype none

module intercon
  (
   input wire 	      CLK,
   input wire 	      RST,

   input wire 	      S0_ACK_O,
   output wire [17:0] S0_ADR_I,
   output wire [15:0] S0_DAT_I,
   input wire [15:0]  S0_DAT_O,
   output wire 	      S0_STB_I,
   output wire 	      S0_WE_I,
   input wire 	      S0_STALL_O,

   input wire 	      S1_ACK_O,
   output wire [17:0] S1_ADR_I,
   output wire [15:0] S1_DAT_I,
   input wire [15:0]  S1_DAT_O,
   output wire 	      S1_STB_I,
   output wire 	      S1_WE_I,
   input wire 	      S1_STALL_O,

   input wire 	      S2_ACK_O,
   output wire [16:0] S2_ADR_I,
   output wire [15:0] S2_DAT_I,
   input wire [15:0]  S2_DAT_O,
   output wire 	      S2_STB_I,
   output wire 	      S2_WE_I,
   input wire 	      S2_STALL_O,

   input wire 	      S3_ACK_O,
   output wire [16:0] S3_ADR_I,
   output wire [15:0] S3_DAT_I,
   input wire [15:0]  S3_DAT_O,
   output wire 	      S3_STB_I,
   output wire 	      S3_WE_I,
   input wire 	      S3_STALL_O,

   input wire 	      S4_ACK_O,
   output wire [16:0] S4_ADR_I,
   output wire [15:0] S4_DAT_I,
   input wire [15:0]  S4_DAT_O,
   output wire 	      S4_STB_I,
   output wire 	      S4_WE_I,
   input wire 	      S4_STALL_O,

   input wire 	      S5_ACK_O,
   output wire [16:0] S5_ADR_I,
   output wire [15:0] S5_DAT_I,
   input wire [15:0]  S5_DAT_O,
   output wire 	      S5_STB_I,
   output wire 	      S5_WE_I,
   input wire 	      S5_STALL_O,

   output wire 	      M0_ACK_I,
   input wire [19:0]  M0_ADR_O,
   output wire [15:0] M0_DAT_I,
   input wire [15:0]  M0_DAT_O,
   input wire 	      M0_STB_O,
   input wire 	      M0_CYC_O,
   input wire 	      M0_WE_O,
   output wire 	      M0_STALL_I,

   output wire 	      M1_ACK_I,
   input wire [19:0]  M1_ADR_O,
   output wire [15:0] M1_DAT_I,
   input wire [15:0]  M1_DAT_O,
   input wire 	      M1_STB_O,
   input wire 	      M1_CYC_O,
   input wire 	      M1_WE_O,
   output wire 	      M1_STALL_I
   );

   wire 	      S_COMBINED_ACK_O;
   wire [19:0] 	      S_COMBINED_ADR_I;
   wire [15:0] 	      S_COMBINED_DAT_I;
   wire [15:0] 	      S_COMBINED_DAT_O;
   wire 	      S_COMBINED_STB_I;
   wire 	      S_COMBINED_WE_I;
   wire 	      S_COMBINED_STALL_O;

   wire 	      M_COMBINED_ACK_I;
   wire [19:0] 	      M_COMBINED_ADR_O;
   wire [15:0] 	      M_COMBINED_DAT_I;
   wire [15:0] 	      M_COMBINED_DAT_O;
   wire 	      M_COMBINED_STB_O;
   wire 	      M_COMBINED_CYC_O;
   wire 	      M_COMBINED_WE_O;
   wire 	      M_COMBINED_STALL_I;

   slave_dispatcher dispatcher
     (
      .CLK(CLK),
      .RST(RST),

      .S0_ACK_O(S0_ACK_O),
      .S0_ADR_I(S0_ADR_I),
      .S0_DAT_I(S0_DAT_I),
      .S0_DAT_O(S0_DAT_O),
      .S0_STB_I(S0_STB_I),
      .S0_WE_I(S0_WE_I),
      .S0_STALL_O(S0_STALL_O),

      .S1_ACK_O(S1_ACK_O),
      .S1_ADR_I(S1_ADR_I),
      .S1_DAT_I(S1_DAT_I),
      .S1_DAT_O(S1_DAT_O),
      .S1_STB_I(S1_STB_I),
      .S1_WE_I(S1_WE_I),
      .S1_STALL_O(S1_STALL_O),

      .S2_ACK_O(S2_ACK_O),
      .S2_ADR_I(S2_ADR_I),
      .S2_DAT_I(S2_DAT_I),
      .S2_DAT_O(S2_DAT_O),
      .S2_STB_I(S2_STB_I),
      .S2_WE_I(S2_WE_I),
      .S2_STALL_O(S2_STALL_O),

      .S3_ACK_O(S3_ACK_O),
      .S3_ADR_I(S3_ADR_I),
      .S3_DAT_I(S3_DAT_I),
      .S3_DAT_O(S3_DAT_O),
      .S3_STB_I(S3_STB_I),
      .S3_WE_I(S3_WE_I),
      .S3_STALL_O(S3_STALL_O),

      .S4_ACK_O(S4_ACK_O),
      .S4_ADR_I(S4_ADR_I),
      .S4_DAT_I(S4_DAT_I),
      .S4_DAT_O(S4_DAT_O),
      .S4_STB_I(S4_STB_I),
      .S4_WE_I(S4_WE_I),
      .S4_STALL_O(S4_STALL_O),

      .S5_ACK_O(S5_ACK_O),
      .S5_ADR_I(S5_ADR_I),
      .S5_DAT_I(S5_DAT_I),
      .S5_DAT_O(S5_DAT_O),
      .S5_STB_I(S5_STB_I),
      .S5_WE_I(S5_WE_I),
      .S5_STALL_O(S5_STALL_O),

      .S_COMBINED_ACK_O(S_COMBINED_ACK_O),
      .S_COMBINED_ADR_I(S_COMBINED_ADR_I),
      .S_COMBINED_DAT_I(S_COMBINED_DAT_I),
      .S_COMBINED_DAT_O(S_COMBINED_DAT_O),
      .S_COMBINED_STB_I(S_COMBINED_STB_I),
      .S_COMBINED_WE_I(S_COMBINED_WE_I),
      .S_COMBINED_STALL_O(S_COMBINED_STALL_O)
      );

   master_arbiter arbiter
     (
      .CLK(CLK),
      .RST(RST),

      .M0_ACK_I(M0_ACK_I),
      .M0_ADR_O(M0_ADR_O),
      .M0_DAT_I(M0_DAT_I),
      .M0_DAT_O(M0_DAT_O),
      .M0_STB_O(M0_STB_O),
      .M0_CYC_O(M0_CYC_O),
      .M0_WE_O(M0_WE_O),
      .M0_STALL_I(M0_STALL_I),

      .M1_ACK_I(M1_ACK_I),
      .M1_ADR_O(M1_ADR_O),
      .M1_DAT_I(M1_DAT_I),
      .M1_DAT_O(M1_DAT_O),
      .M1_STB_O(M1_STB_O),
      .M1_CYC_O(M1_CYC_O),
      .M1_WE_O(M1_WE_O),
      .M1_STALL_I(M1_STALL_I),

      .M_COMBINED_ACK_I(M_COMBINED_ACK_I),
      .M_COMBINED_ADR_O(M_COMBINED_ADR_O),
      .M_COMBINED_DAT_I(M_COMBINED_DAT_I),
      .M_COMBINED_DAT_O(M_COMBINED_DAT_O),
      .M_COMBINED_STB_O(M_COMBINED_STB_O),
      .M_COMBINED_CYC_O(M_COMBINED_CYC_O),
      .M_COMBINED_WE_O(M_COMBINED_WE_O),
      .M_COMBINED_STALL_I(M_COMBINED_STALL_I)
   );

   assign M_COMBINED_ACK_I = S_COMBINED_ACK_O;
   assign M_COMBINED_DAT_I = S_COMBINED_DAT_O;
   assign M_COMBINED_STALL_I = S_COMBINED_STALL_O;

   assign S_COMBINED_ADR_I = M_COMBINED_ADR_O;
   assign S_COMBINED_DAT_I = M_COMBINED_DAT_O;
   assign S_COMBINED_STB_I = M_COMBINED_STB_O && M_COMBINED_CYC_O;
   assign S_COMBINED_WE_I = M_COMBINED_WE_O;
endmodule // intercon
