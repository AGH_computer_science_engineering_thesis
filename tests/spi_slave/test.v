`default_nettype none
`timescale 1ns/1ns

`include "messages.vh"

`ifndef MASTER_OPERATIONS_COUNT
 `error_MASTER_OPERATIONS_COUNT_must_be_defined
; /* Cause syntax error */
`endif

`ifndef ROM_WORDS_COUNT
 `error_ROM_WORDS_COUNT_must_be_defined
; /* Cause syntax error */
`endif

`ifndef SIMULATION
 `error_SIMULATION_not_defined
; /* Cause syntax error */
`endif

module spi_test();
   wire        M_ACK_I;
   wire        M_CLK_I;
   wire [19:0] M_ADR_O;
   wire [15:0] M_DAT_I;
   wire [15:0] M_DAT_O;
   wire        M_SEL_O; /* Ignored, assumed always high */
   wire        M_RST_I;
   wire        M_STB_O;
   wire        M_CYC_O;
   wire        M_WE_O;
   wire        M_STALL_I;

   wire        S_ACK_O;
   wire        S_CLK_I;
   wire [8:0]  S_ADR_I;
   wire [15:0] S_DAT_I;
   wire [15:0] S_DAT_O;
   wire        S_RST_I;
   wire        S_STB_I;
   wire        S_WE_I;
   wire        S_STALL_O;

   /* Non-wishbone */
   wire        sdo;
   wire        sdi;
   wire        sck;
   wire        ss_n;

   wire        M_finished;

   master_model
     #(
       .MASTER_NR(0),
       .OPERATIONS_FILE("operations.mem"),
       .OPERATIONS_COUNT(`MASTER_OPERATIONS_COUNT)
       ) master
       (
	.ACK_I(M_ACK_I),
 	.CLK_I(M_CLK_I),
	.ADR_O(M_ADR_O),
	.DAT_I(M_DAT_I),
	.DAT_O(M_DAT_O),
	.SEL_O(M_SEL_O),
 	.RST_I(M_RST_I),
	.STB_O(M_STB_O),
	.CYC_O(M_CYC_O),
	.WE_O(M_WE_O),
 	.STALL_I(M_STALL_I),

	.finished(M_finished)
	);

   spi_slave slave
     (
      .ACK_O(S_ACK_O),
      .CLK_I(S_CLK_I),
      .ADR_I(S_ADR_I),
      .DAT_I(S_DAT_I),
      .DAT_O(S_DAT_O),
      .RST_I(S_RST_I),
      .STB_I(S_STB_I),
      .WE_I(S_WE_I),
      .STALL_O(S_STALL_O),

      .sdo(sdo),
      .sdi(sdi),
      .sck(sck),
      .ss_n(ss_n)
      );

   W25Q16BV_flash
     #(
       .BYTES_TO_INITIALIZE(`ROM_WORDS_COUNT * 2),
       .INITIAL_CONTENTS_FILE("rom.mem")
       ) flash
       (
	.sdo(sdo),
	.sdi(sdi),
	.sck(sck),
	.ss_n(ss_n)
	);

   reg 	       CLK;
   reg 	       RST;

   assign M_ACK_I = S_ACK_O;
   assign M_CLK_I = CLK;
   assign M_DAT_I = S_DAT_O;
   assign M_RST_I = RST;
   assign M_STALL_I = S_STALL_O;

   assign S_CLK_I = CLK;
   assign S_ADR_I = M_ADR_O[8:0]; /* Ignore 11 topmost bits */
   assign S_DAT_I = M_DAT_O;
   assign S_RST_I = RST;
   assign S_STB_I = M_STB_O && M_CYC_O;
   assign S_WE_I = M_WE_O;

   integer     i;

   initial begin
      CLK <= 0;
      RST <= 1;

      for (i = 0; i < 600; i++) begin
	 #100; /* longer wait time so that power-up requires less idle ticks */

	 CLK <= ~CLK;

	 if (CLK)
	   RST <= 0;

	 if (M_finished)
	   $finish;
      end

      $display("error: master hasn't finished its operations in 300 ticks");
      $finish;
   end
endmodule // spi_test
