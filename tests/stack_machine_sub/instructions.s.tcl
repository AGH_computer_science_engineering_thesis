### store 2 values to memory, load them back,
### substract them and store the result

set_sp 0

const 68996288
store h1EEE0
const 540904416
store h1EEE4

const 8

load h1EEE0
load h1EEE4
# substracting 540904416 from 68996288 should yield -471908128
sub
# will write to h1EEE0 + 8 = h1EEE8
store+ h1EEE0

halt
