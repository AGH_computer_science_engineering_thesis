### make memory reads and writes of bytes, words and dwords at both
### aligned and unaligned locations

## set up stack
set_sp 0

## store value h9A785634 at h100 writing 1 byte at a time

# write first 2 bytes using storeb
const h34
storeb h000100

# try putting some garbage in higher 3 bytes of our constant - it should
# not get written anyway
const h9ABA9E56
storeb h000101

# write another 2 bytes using storeb+
const 2
const h78
storeb+ h100

const 3
const h9A
storeb+ h100


## test single-byte loads

# sign-extend read value h9A (as hFFFFFF9A) and store it at h104
loadbsx h00103
store h00104

# sign-extend read value h78 (as h00000078) and store it at h108
const 2
loadbsx+ h00100
store h00108

# zero-extend read value h9A (as h0000009A) and store it at h10C
const 3
loadbzx+ h00100
store h0010C

# zero-extend read value h78 (as h00000078) and store it at h110
loadbzx h00102
store h00110


## store values hD4C32B1A and h8877FFEE at h200 and h204
## writing 2 bytes at a time

# make aligned writes at h200 and h206
# only byte 1A matters - higher one will be overwritten later
const h481A
storew h00200

# only byte 88 matters - lower one will be overwritten later
const 6
const h8805
storew+ h00200

# make unaligned writes at h203, h201 and h205
const hEED4
storew h00203

const 1
const hC32B
storew+ h00200

# only bytes 77FF matter - higher ones won't be written anyway
const h71F577FF
storew h00205


## test 2-word loads

# sign-extend read value h2B1A (as h00002B1A) and store it at h208
const 0
loadwsx+ h00200
store h00208

# sign-extend read value hC32B (as hFFFFC32B) and store it at h20C
loadwsx h00201
store h0020C

# zero-extend read value hD4C3 (as h0000D4C3) and store it at h210
loadwzx h00202
store h00210

# zero-extend read value h77FF (as h000077FF) and store it at h214
const 5
loadwzx+ h00200
store h00214


## test unaligned 4-byte memory writes

# write values h92837400, hBECD0001 and h000044AF at h300, h304 and h308
const 0
const 0
const 0
storeb h300
storeb h305
storew h30A

const h01928374
store h301

const 6
const h44AFBECD
store+ h300


## test unaligned 4-byte memory reads

# read value hCD000192 and write it at h30C
load h303
store h30C

# read value h0044AFBE and write it at h310
const 7
load+ h300
store h310

halt
