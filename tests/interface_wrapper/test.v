/* adapted from self test */
`default_nettype none

`include "messages.vh"

`ifndef MASTER_OPERATIONS_COUNT
 `error_MASTER_OPERATIONS_COUNT_must_be_defined
; /* Cause syntax error */
`endif

`ifndef SIMULATION
 `error_SIMULATION_not_defined
; /* Cause syntax error */
`endif

module interface_wrapper_test();
   wire        M_CLK_I;
   wire        M_RST_I;

   wire        M_RAW_ACK_I;
   wire        M_RAW_ERR_I; /* Not used yet, always low */
   wire [20:0] M_RAW_ADR_O;
   wire [31:0] M_RAW_DAT_I;
   wire [31:0] M_RAW_DAT_O;
   wire [3:0]  M_RAW_SEL_O; /* This is being worked on */
   wire        M_RAW_STB_O;
   wire        M_RAW_CYC_O;
   wire        M_RAW_WE_O;
   wire        M_RAW_STALL_I;

   wire        M_WRAPPED_ACK_I;
   wire [19:0] M_WRAPPED_ADR_O;
   wire [15:0] M_WRAPPED_DAT_I;
   wire [15:0] M_WRAPPED_DAT_O;
   wire        M_WRAPPED_STB_O;
   wire        M_WRAPPED_CYC_O;
   wire        M_WRAPPED_WE_O;
   wire        M_WRAPPED_STALL_I;

   wire        S_ACK_O;
   wire        S_CLK_I;
   wire [19:0] S_ADR_I;
   wire [15:0] S_DAT_I;
   wire [15:0] S_DAT_O;
   wire        S_SEL_I; /* Always high */
   wire        S_RST_I;
   wire        S_STB_I;
   wire        S_WE_I;
   wire        S_STALL_O;

   /* Non-wishbone */
   wire        M_finished;

   master_model
     #(
       .MASTER_NR(0),
       .WORD_SIZE(4),
       .SEL_LINES(4),
       .ADR_BITS(21),
       .OPERATIONS_FILE("operations.mem"),
       .OPERATIONS_COUNT(`MASTER_OPERATIONS_COUNT)
       ) master
       (
	.ACK_I(M_RAW_ACK_I),
 	.CLK_I(M_CLK_I),
	.ADR_O(M_RAW_ADR_O),
	.DAT_I(M_RAW_DAT_I),
	.DAT_O(M_RAW_DAT_O),
	.SEL_O(M_RAW_SEL_O),
 	.RST_I(M_RST_I),
	.STB_O(M_RAW_STB_O),
	.CYC_O(M_RAW_CYC_O),
	.WE_O(M_RAW_WE_O),
 	.STALL_I(M_RAW_STALL_I),

	.finished(M_finished)
	);

   memory_slave_model
     #(
       .SLAVE_NR(0),
       .WORD_SIZE(2),
       .ADR_BITS(20)
       ) slave
       (
	.ACK_O(S_ACK_O),
      	.CLK_I(S_CLK_I),
	.ADR_I(S_ADR_I),
	.DAT_I(S_DAT_I),
	.DAT_O(S_DAT_O),
	.SEL_I(S_SEL_I),
      	.RST_I(S_RST_I),
      	.STB_I(S_STB_I),
      	.WE_I(S_WE_I),
	.STALL_O(S_STALL_O)
	);

interface_wrapper wrapper
   (
    .CLK_I(M_CLK_I),
    .RST_I(M_RST_I),

    .RAW_ACK_I(M_RAW_ACK_I),
    .RAW_ERR_I(M_RAW_ERR_I),
    .RAW_ADR_O(M_RAW_ADR_O),
    .RAW_DAT_I(M_RAW_DAT_I),
    .RAW_DAT_O(M_RAW_DAT_O),
    .RAW_SEL_O(M_RAW_SEL_O),
    .RAW_STB_O(M_RAW_STB_O),
    .RAW_CYC_O(M_RAW_CYC_O),
    .RAW_WE_O(M_RAW_WE_O),
    .RAW_STALL_I(M_RAW_STALL_I),

    .WRAPPED_ACK_I(M_WRAPPED_ACK_I),
    .WRAPPED_ADR_O(M_WRAPPED_ADR_O),
    .WRAPPED_DAT_I(M_WRAPPED_DAT_I),
    .WRAPPED_DAT_O(M_WRAPPED_DAT_O),
    .WRAPPED_STB_O(M_WRAPPED_STB_O),
    .WRAPPED_CYC_O(M_WRAPPED_CYC_O),
    .WRAPPED_WE_O(M_WRAPPED_WE_O),
    .WRAPPED_STALL_I(M_WRAPPED_STALL_I)
    );

   reg 	       CLK;
   reg 	       RST;

   assign M_CLK_I = CLK;
   assign M_RST_I = RST;

   assign M_WRAPPED_ACK_I = S_ACK_O;
   assign M_WRAPPED_DAT_I = S_DAT_O;
   assign M_WRAPPED_STALL_I = S_STALL_O;

   assign S_CLK_I = CLK;
   assign S_ADR_I = M_WRAPPED_ADR_O;
   assign S_DAT_I = M_WRAPPED_DAT_O;
   assign S_SEL_I = 1;
   assign S_RST_I = RST;
   assign S_STB_I = M_WRAPPED_STB_O && M_WRAPPED_CYC_O;
   assign S_WE_I = M_WRAPPED_WE_O;

   integer     i;

   initial begin
      CLK <= 0;
      RST <= 1;

      for (i = 0; i < 2000; i++) begin
	 #1;

	 CLK <= ~CLK;

	 if (CLK)
	   RST <= 0;

	 if (M_finished)
	   $finish;
      end // for (i = 0; i < 2000; i++)

      $display("error: master hasn't finished its operations in 1000 ticks");
      $finish;
   end
endmodule // interface_wrapper_test
