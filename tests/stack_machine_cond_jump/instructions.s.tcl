## also look at stack_machine_jump test

## we're going to write numbers from 0 to 7 to addresses h400 - h41C

# this will translate to 1 16-bit instruction
set_sp 0

## set up the counter (1 16-bit instruction)
const 0

## this is the point we later jump to, address 4
tee
tee
## compute address:  counter * 4 + h400 and save counter to it
const 4
mul
swap
store+ h400

## increase counter by 1
const 1
add
## compare value of counter to 8
tee
const 8
sub
## loop if counter != 8
cond_jump 4

halt
