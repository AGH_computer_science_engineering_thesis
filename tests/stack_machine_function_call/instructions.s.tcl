### call a simple function, that sums its 2 arguments

## store sth at h1000 to later check if calee restored it properly
# 1 16-bit instruction
const h23
# 2 16-bit instructions
store h1000

# function arguments - each loaded with 1 16-bit instruction
const h32
const h14
# also 1 16-bit instruction
call 18

## this executes after return - store the computed difference at h400
# this takes 2 16-bit instructions
store h400
# and this takes 1 16-bit instruction
halt

# address 18 here

## function frame will look like this:
##  *****higher addresses*****
##          argument1
##          argument2
##             ...
##        return address
##            local1
##            local2
##             ...
##    previous frame address
##  *****lower  addresses*****

## push the arguments out of r0 and r1 to memory (+ claim space for
## backed stack frame address and backed return address);
## if we had n local variables, we'd repeat that n more times
const 0
const 0
const 0

## with this we get the address to reference our locals from into r1
get_frame
tee

## we get our previous frame and back it up as first local
load h1000
store+ h0

## we now store our new function frame
store h1000

## here the actual function body starts

## fetch the first argument
load h1000
load+ hC

## fetch the second one
load h1000
load+ h8

sub

## function exit - store the result at the the top of current frame
load h1000
swap
store+ hC

## put the return address just below result
load h1000
tee
tee
load+ h4
store+ h8

## restore old frame
load+ 0
store h1000

## discard current frame
drop
drop
drop
drop

## return
ret
