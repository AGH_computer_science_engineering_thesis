`default_nettype none

`include "messages.vh"

`ifndef SIMULATION
 `error_SIMULATION_not_defined
; /* Cause syntax error */
`endif

module div_test();
   reg clock;
   reg start;
   reg [15:0] dividend;
   reg [15:0] divisor;

   wire [15:0] quotient;
   wire [15:0] remainder;
   wire        done;

   div
  #(
    .WIDTH(16)
    ) div
    (
     .clock(clock),
     .start(start),
     .dividend(dividend),
     .divisor(divisor),
     .quotient(quotient),
     .remainder(remainder),
     .done(done)
     );

   integer     seed;
   integer     progress;
   reg [15:0]  new_divisor;
   reg [16:0]  max_divisor;

   initial begin
      seed <= 0;
      progress <= 0;
      clock <= 0;
   end

   // initial
   //   $monitor("[%t] clock is %b", $time, clock);

   always #1
     clock <= ~clock;

   always @ (posedge clock) begin
      if (progress == 0) begin
	 start <= 1;
	 dividend <= 15;
	 divisor <= 3;
	 progress <= progress + 1;
      end else begin
	 if (done) begin
	    if (dividend / divisor === quotient && dividend % divisor === remainder) begin
	       `DBG(("%0d/%0d computed as %0d r %0d", dividend, divisor, quotient, remainder));
	    end else begin
	       `MSG(("error: %0d/%0d computed as %0d r %0d",
		     dividend, divisor, quotient, remainder));
	    end
	    start <= 1;
	    case (progress)
	      1 : begin
		 dividend <= 3;
		 divisor <= 4;
	      end
	      2 : begin
		 dividend <= 65535;
		 divisor <= 65534;
	      end
	      3 : begin
		 dividend <= 1024;
		 divisor <= 4;
	      end
	      4 : begin
		 dividend <= 319;
		 divisor <= 17;
	      end
	      default : begin
		 if (progress == 500)
		   $finish;
		 else if (progress > 400)
		   max_divisor = 2**16;
		 else if (progress > 200)
		   max_divisor = 2**10;
		 else
 		   max_divisor = 2**5;

		 for (new_divisor = $urandom(seed) % max_divisor;
		      new_divisor == 0;
		      new_divisor = $urandom(seed) % max_divisor)
		   ;

		 dividend <= $urandom(seed);
		 divisor <= new_divisor;
	      end
	    endcase // case (progress)
	    progress <= progress + 1;
	 end else begin // if (done)
	    start <= 0;
	 end // else: !if(done)
      end // else: !if(progress == 0)
   end // always @ (posedge clock)
endmodule // div_test
