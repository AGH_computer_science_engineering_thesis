`default_nettype none
`timescale 1ns/1ns

`include "messages.vh"

`ifndef MASTER_OPERATIONS_COUNT
 `error_MASTER_OPERATIONS_COUNT_must_be_defined
; /* Cause syntax error */
`endif

`ifndef SIMULATION
 `error_SIMULATION_not_defined
; /* Cause syntax error */
`endif

module sram_slave_test();
   /* sram chip pins */
   wire [17:0] sram_addr;
   wire [15:0] sram_io;
   wire        sram_cs_not;
   wire        sram_oe_not;
   wire        sram_we_not;

   /* slave wishbone interface */
   wire        S_ACK_O;
   wire        S_CLK_I;
   wire [17:0] S_ADR_I;
   wire [15:0] S_DAT_I;
   wire [15:0] S_DAT_O;
   wire        S_RST_I;
   wire        S_STB_I;
   wire        S_WE_I;
   wire        S_STALL_O;

   /* master wishbone interface */
   wire        M_ACK_I;
   wire        M_CLK_I;
   wire [19:0] M_ADR_O;
   wire [15:0] M_DAT_I;
   wire [15:0] M_DAT_O;
   wire        M_RST_I;
   wire        M_STB_O;
   wire        M_CYC_O;
   wire        M_WE_O;
   wire        M_STALL_I;

   /* Non-wishbone */
   wire        M_finished;

   master_model
     #(
       .MASTER_NR(0),
       .OPERATIONS_FILE("operations.mem"),
       .OPERATIONS_COUNT(`MASTER_OPERATIONS_COUNT)
       ) master
       (
	.ACK_I(M_ACK_I),
 	.CLK_I(M_CLK_I),
	.ADR_O(M_ADR_O),
	.DAT_I(M_DAT_I),
	.DAT_O(M_DAT_O),
 	.RST_I(M_RST_I),
	.STB_O(M_STB_O),
	.CYC_O(M_CYC_O),
	.WE_O(M_WE_O),
 	.STALL_I(M_STALL_I),

	.finished(M_finished)
	);

   K6R4016V1D_TC10_sram sram
     (
      .sram_addr(sram_addr),
      .sram_io(sram_io),
      .sram_cs_not(sram_cs_not),
      .sram_oe_not(sram_oe_not),
      .sram_we_not(sram_we_not)
      );

   sram_slave slave
     (
      .sram_addr(sram_addr),
      .sram_io(sram_io),
      .sram_cs_n(sram_cs_not),
      .sram_oe_n(sram_oe_not),
      .sram_we_n(sram_we_not),

      .ACK_O(S_ACK_O),
      .CLK_I(S_CLK_I),
      .ADR_I(S_ADR_I),
      .DAT_I(S_DAT_I),
      .DAT_O(S_DAT_O),
      .RST_I(S_RST_I),
      .STB_I(S_STB_I),
      .WE_I(S_WE_I),
      .STALL_O(S_STALL_O)
      );

   reg 	       CLK;
   reg 	       RST;

   assign M_ACK_I = S_ACK_O;
   assign M_CLK_I = CLK;
   assign M_DAT_I = S_DAT_O;
   assign M_RST_I = RST;
   assign M_STALL_I = S_STALL_O;

   assign S_CLK_I = CLK;
   assign S_ADR_I = M_ADR_O[17:0]; /* Ignore 2 topmost bits */
   assign S_DAT_I = M_DAT_O;
   assign S_RST_I = RST;
   assign S_STB_I = M_STB_O && M_CYC_O;
   assign S_WE_I = M_WE_O;

   integer     i;

   initial begin
      CLK <= 0;
      RST <= 1;

      for (i = 0; i < 600; i++) begin
	 #7;

	 CLK <= ~CLK;

	 if (CLK)
	   RST <= 0;

	 if (M_finished)
	   $finish;
      end

      $display("error: master hasn't finished its operations in 300 ticks");
      $finish;
   end
endmodule // sram_slave_test
