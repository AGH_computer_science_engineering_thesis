### store 2 values to memory, load them back, divide one by another and store
### the result (remainder); this is analogous to addition and substraction tests

set_sp 0

const 777681520
store h1EEE0
const 3721
store h1EEE4

load h1EEE0
load h1EEE4
# dividing 777681520 by 3721 should yield 208997 r 3683
rem
store h1EEE8

halt
