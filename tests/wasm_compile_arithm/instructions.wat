(module
 (memory 0 2)
 (func $main
       ;; write 0xFFEFE021 at MEMORY_BOTTOM_ADDR + 0x0
       (i32.store offset=0x0 align=4 (i32.const 0x0)
       		  (i32.sub (i32.const 0x42143) (i32.const 0x144122)))
       ;; write 0x16D3B7 at MEMORY_BOTTOM_ADDR + 0x4
       (i32.store offset=0x0 align=4 (i32.const 0x4)
       		  (i32.add (i32.const 0x7109D) (i32.const 0xFC31A)))
       ;; write 0xED8 at MEMORY_BOTTOM_ADDR + 0x8
       (i32.store offset=0x0 align=4 (i32.const 0x8)
       		  (i32.div_u (i32.const 0x193E324F) (i32.const 0x1B342)))
       ;; write 0x3C773E7C at MEMORY_BOTTOM_ADDR + 0xC
       (i32.store offset=0x0 align=4 (i32.const 0xC)
       		  (i32.mul (i32.const 0x38F2C) (i32.const 0x10FD))))
 (export "main" (func $main)))
