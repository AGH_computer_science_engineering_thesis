## also look at stack_machine_cond_jump test

## we're going to write numbers from 0 to 639 at addresses h100000 to h1009FC
## and then write non-zero value at h100A00

# this will translate to 2 16-bit instructions
set_sp h100000

## set up the counter (1 16-bit instruction)
const 0

## this is the point we later jump to, address 6
tee
tee
## compute address:  counter * 4 + h100000
const 4
mul
swap
store+ h100000

## increase counter by 1
const 1
add
## compare value of counter to 640
tee
const 640
sub
## loop if counter != 640
cond_jump 6

## write hFFFFFFFF to address h100A00 (the point is to write a non-zero value
## there, but because we're only using aligned 4-byte writes in this test,
## we'll write to h100A00 and h100A02, both being mapped as the VGA power-on
## register - what matters is the later write, so at least one of higher 16
## bits of written value has to be non-zero)
const -1
store h100A00

halt
