`default_nettype none
`timescale 1ns/1ns

`include "messages.vh"

`ifndef SIMULATION
 `error_SIMULATION_not_defined
; /* Cause syntax error */
`endif

`ifndef ROM_WORDS_COUNT
 `error_ROM_WORDS_COUNT_must_be_defined
; /* Cause syntax error */
`endif

module soc_test();
   wire [9:0] image_writes;

   reg 	      clock_100mhz;
   reg 	      reset;

   wire       led1;
   wire       led2;

   soc_with_peripherals
     #(
       .FONT_FILE("../../design/font.mem"),
       .EMBEDDED_ROM_WORDS_COUNT(`ROM_WORDS_COUNT),
       .EMBEDDED_ROM_FILE("instructions.mem")
       ) soc
       (
	.clock_100mhz(clock_100mhz),

	.button1(!reset),
	.button2(1'b1),

	.led1(led1),
	.led2(led2),

	.image_writes(image_writes)
	);

   integer     i;

   initial begin
      reset <= 1;
      clock_100mhz <= 0;

      for (i = 0; i < 5_000_000; i++) begin
	 #5;

	 if (clock_100mhz)
	   reset <= 0;

	 clock_100mhz <= ~clock_100mhz;
      end

      if (led1)
	`MSG(("error: stack machine in soc hasn't finished working in 25ms"));
      else
	`MSG(("error: nothing got displayed in 25ms"));
   end

   always @ (image_writes) begin
      if (image_writes)
	$finish;
   end
endmodule // soc_test
