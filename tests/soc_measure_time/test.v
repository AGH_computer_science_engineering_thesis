`default_nettype none
`timescale 1ns/1ns

`include "messages.vh"

`ifndef SIMULATION
 `error_SIMULATION_not_defined
; /* Cause syntax error */
`endif

`ifndef ROM_WORDS_COUNT
 `error_ROM_WORDS_COUNT_must_be_defined
; /* Cause syntax error */
`endif

module soc_test();
   wire [9:0] image_writes;

   reg 	      clock_100mhz;
   reg 	      reset;

   wire       led1;
   wire       led2;

   soc_with_peripherals
     #(
       .FONT_FILE("../../design/font.mem"),
       .EMBEDDED_ROM_WORDS_COUNT(`ROM_WORDS_COUNT),
       .EMBEDDED_ROM_FILE("instructions.mem")
       ) soc
       (
	.clock_100mhz(clock_100mhz),

	.button1(!reset),
	.button2(1'b1),

	.led1(led1),
	.led2(led2),

	.image_writes(image_writes)
	);

   integer     i;
   integer     current_time;

   initial begin
      reset <= 1;
      clock_100mhz <= 0;

      for (i = 0; i < 25_000; i++) begin
	 #5;

	 if (clock_100mhz)
	   reset <= 0;

	 clock_100mhz <= ~clock_100mhz;

	 /*
	  * Soc's clock is 12.5 MHz. One tick of that clock takes 80 ns.
	  * This means 1000th, 2000th and 3000th ticks happen at
	  * 80_000, 160_000 and 240_000 ns, respectively.
	  */
	 current_time = $time;
	 if (current_time < 40_000) begin
	    if (!led2) begin
	       `MSG(("error: led2 on before 1000 timer ticks passed"));
	       $finish;
	    end
	 end
	 if (current_time > 50_000 && current_time < 80_000) begin
	    if (led2) begin
	       `MSG(("error: led2 not on, while it should be"));
	       $finish;
	    end
	 end
	 if (current_time > 130_000) begin
	    if (!led2) begin
	       `MSG(("error: led2 hasn't been switched off on time"));
	       $finish;
	    end
	 end
      end // for (i = 0; i < 12_500; i++)

      if (led1)
	`MSG(("error: stack machine in soc hasn't finished working in 125us"));
      $finish;
   end // initial begin
endmodule // soc_test
