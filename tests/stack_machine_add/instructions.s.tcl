### store 2 values to memory, load them back, add them and store the result

set_sp 0

const 12345678
store h1EEE0
const 40302010
store h1EEE4

const 8

load h1EEE0
load h1EEE4
# adding 40302010 to 12345678 should yield 52647688
add
# will write to h1EEE0 + 8 = h1EEE8
store+ h1EEE0

halt
