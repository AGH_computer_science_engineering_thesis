`default_nettype none

`include "messages.vh"

`ifndef MASTER0_OPERATIONS_COUNT
 `error_MASTER0_OPERATIONS_COUNT_must_be_defined
; /* Cause syntax error */
`endif

`ifndef MASTER1_OPERATIONS_COUNT
 `error_MASTER1_OPERATIONS_COUNT_must_be_defined
; /* Cause syntax error */
`endif

`ifndef SIMULATION
 `error_SIMULATION_not_defined
; /* Cause syntax error */
`endif

module master_arbiter_test();
   wire        M0_ACK_I;
   wire [19:0] M0_ADR_O;
   wire [15:0] M0_DAT_I;
   wire [15:0] M0_DAT_O;
   wire        M0_SEL_O; /* Ignored, assumed always high */
   wire        M0_STB_O;
   wire        M0_CYC_O;
   wire        M0_WE_O;
   wire        M0_STALL_I;

   wire        M1_ACK_I;
   wire [19:0] M1_ADR_O;
   wire [15:0] M1_DAT_I;
   wire [15:0] M1_DAT_O;
   wire        M1_SEL_O; /* Ignored, assumed always high */
   wire        M1_STB_O;
   wire        M1_CYC_O;
   wire        M1_WE_O;
   wire        M1_STALL_I;

   wire        S_ACK_O;
   wire [19:0] S_ADR_I;
   wire [15:0] S_DAT_I;
   wire [15:0] S_DAT_O;
   wire        S_SEL_I; /* Always high */
   wire        S_STB_I;
   wire        S_WE_I;
   wire        S_STALL_O;

   wire        M_COMBINED_ACK_I;
   wire        M_COMBINED_CLK_I;
   wire [19:0] M_COMBINED_ADR_O;
   wire [15:0] M_COMBINED_DAT_I;
   wire [15:0] M_COMBINED_DAT_O;
   wire        M_COMBINED_RST_I;
   wire        M_COMBINED_STB_O;
   wire        M_COMBINED_CYC_O;
   wire        M_COMBINED_WE_O;
   wire        M_COMBINED_STALL_I;

   reg 	       CLK;
   reg 	       RST;

   /* Non-wishbone */
   wire        M0_finished;
   wire        M1_finished;

   master_model
     #(
       .MASTER_NR(0),
       .WORD_SIZE(2),
       .ADR_BITS(20),
       .OPERATIONS_FILE("operations0.mem"),
       .OPERATIONS_COUNT(`MASTER0_OPERATIONS_COUNT)
       ) master0
       (
	.ACK_I(M0_ACK_I),
 	.CLK_I(CLK),
	.ADR_O(M0_ADR_O),
	.DAT_I(M0_DAT_I),
	.DAT_O(M0_DAT_O),
	.SEL_O(M0_SEL_O),
 	.RST_I(RST),
	.STB_O(M0_STB_O),
	.CYC_O(M0_CYC_O),
	.WE_O(M0_WE_O),
 	.STALL_I(M0_STALL_I),

	.finished(M0_finished)
	);

   master_model
     #(
       .MASTER_NR(1),
       .WORD_SIZE(2),
       .ADR_BITS(20),
       .OPERATIONS_FILE("operations1.mem"),
       .OPERATIONS_COUNT(`MASTER1_OPERATIONS_COUNT)
       ) master1
       (
	.ACK_I(M1_ACK_I),
 	.CLK_I(CLK),
	.ADR_O(M1_ADR_O),
	.DAT_I(M1_DAT_I),
	.DAT_O(M1_DAT_O),
	.SEL_O(M1_SEL_O),
 	.RST_I(RST),
	.STB_O(M1_STB_O),
	.CYC_O(M1_CYC_O),
	.WE_O(M1_WE_O),
 	.STALL_I(M1_STALL_I),

	.finished(M1_finished)
	);

   memory_slave_model
     #(
       .SLAVE_NR(0),
       .WORD_SIZE(2),
       .ADR_BITS(20)
       ) slave
       (
	.ACK_O(S_ACK_O),
      	.CLK_I(CLK),
	.ADR_I(S_ADR_I),
	.DAT_I(S_DAT_I),
	.DAT_O(S_DAT_O),
	.SEL_I(S_SEL_I),
      	.RST_I(RST),
      	.STB_I(S_STB_I),
      	.WE_I(S_WE_I),
	.STALL_O(S_STALL_O)
	);

   master_arbiter arbiter
     (
      .CLK(CLK),
      .RST(RST),

      .M0_ACK_I(M0_ACK_I),
      .M0_ADR_O(M0_ADR_O),
      .M0_DAT_I(M0_DAT_I),
      .M0_DAT_O(M0_DAT_O),
      .M0_STB_O(M0_STB_O),
      .M0_CYC_O(M0_CYC_O),
      .M0_WE_O(M0_WE_O),
      .M0_STALL_I(M0_STALL_I),

      .M1_ACK_I(M1_ACK_I),
      .M1_ADR_O(M1_ADR_O),
      .M1_DAT_I(M1_DAT_I),
      .M1_DAT_O(M1_DAT_O),
      .M1_STB_O(M1_STB_O),
      .M1_CYC_O(M1_CYC_O),
      .M1_WE_O(M1_WE_O),
      .M1_STALL_I(M1_STALL_I),

      .M_COMBINED_ACK_I(M_COMBINED_ACK_I),
      .M_COMBINED_ADR_O(M_COMBINED_ADR_O),
      .M_COMBINED_DAT_I(M_COMBINED_DAT_I),
      .M_COMBINED_DAT_O(M_COMBINED_DAT_O),
      .M_COMBINED_STB_O(M_COMBINED_STB_O),
      .M_COMBINED_CYC_O(M_COMBINED_CYC_O),
      .M_COMBINED_WE_O(M_COMBINED_WE_O),
      .M_COMBINED_STALL_I(M_COMBINED_STALL_I)
   );

   assign M_COMBINED_ACK_I = S_ACK_O;
   assign M_COMBINED_DAT_I = S_DAT_O;
   assign M_COMBINED_STALL_I = S_STALL_O;

   assign S_ADR_I = M_COMBINED_ADR_O[19:0];
   assign S_DAT_I = M_COMBINED_DAT_O;
   assign S_SEL_I = 1;
   assign S_STB_I = M_COMBINED_STB_O && M_COMBINED_CYC_O;
   assign S_WE_I = M_COMBINED_WE_O;

   integer     i;

   initial begin
      CLK <= 0;
      RST <= 1;

      for (i = 0; i < 1000; i++) begin
	 #1;

	 CLK <= ~CLK;

	 if (CLK)
	   RST <= 0;

	 if (M0_finished && M1_finished)
	   $finish;
      end

      if (!M0_finished)
	$display("error: master 0 hasn't finished its operations in 500 ticks");

      if (!M1_finished)
	$display("error: master 1 hasn't finished its operations in 500 ticks");

      $finish;
   end
endmodule // master_arbiter_test
