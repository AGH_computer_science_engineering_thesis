set_sp 0

const h0000DEAD
const h0000BEEF

swap

## Because values were swapped, h0000DEAD should get written, first, at lower
## address (h100) and h0000BEEF should be written next (at h200)
store h000100
store h000200

halt
