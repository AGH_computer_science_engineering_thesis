`default_nettype none

`include "messages.vh"

`ifndef MASTER0_OPERATIONS_COUNT
 `error_MASTER0_OPERATIONS_COUNT_must_be_defined
; /* Cause syntax error */
`endif

`ifndef MASTER1_OPERATIONS_COUNT
 `error_MASTER1_OPERATIONS_COUNT_must_be_defined
; /* Cause syntax error */
`endif

`ifndef SIMULATION
 `error_SIMULATION_not_defined
; /* Cause syntax error */
`endif

module intercon_test();
   reg 	       CLK;
   reg 	       RST;

   wire        M0_ACK_I,   M1_ACK_I;
   wire [19:0] M0_ADR_O,   M1_ADR_O;
   wire [15:0] M0_DAT_I,   M1_DAT_I;
   wire [15:0] M0_DAT_O,   M1_DAT_O;
   wire        M0_SEL_O,   M1_SEL_O; /* Ignored, assumed always high */
   wire        M0_STB_O,   M1_STB_O;
   wire        M0_CYC_O,   M1_CYC_O;
   wire        M0_WE_O,    M1_WE_O;
   wire        M0_STALL_I, M1_STALL_I;

   wire        S0_ACK_O,   S1_ACK_O,   S2_ACK_O,
	       S3_ACK_O,   S4_ACK_O,   S5_ACK_O;
   wire        S0_CLK_I,   S1_CLK_I,   S2_CLK_I,
	       S3_CLK_I,   S4_CLK_I,   S5_CLK_I;
   wire [17:0] S0_ADR_I,   S1_ADR_I;
   wire [16:0] S2_ADR_I,   S3_ADR_I,   S4_ADR_I,   S5_ADR_I;
   wire [15:0] S0_DAT_I,   S1_DAT_I,   S2_DAT_I,
	       S3_DAT_I,   S4_DAT_I,   S5_DAT_I;
   wire [15:0] S0_DAT_O,   S1_DAT_O,   S2_DAT_O,
	       S3_DAT_O,   S4_DAT_O,   S5_DAT_O;
   wire        S0_SEL_I,   S1_SEL_I,   S2_SEL_I,
	       S3_SEL_I,   S4_SEL_I,   S5_SEL_I; /* Always high */
   wire        S0_RST_I,   S1_RST_I,   S2_RST_I,
	       S3_RST_I,   S4_RST_I,   S5_RST_I;
   wire        S0_STB_I,   S1_STB_I,   S2_STB_I,
	       S3_STB_I,   S4_STB_I,   S5_STB_I;
   wire        S0_WE_I,    S1_WE_I,    S2_WE_I,
	       S3_WE_I,    S4_WE_I,    S5_WE_I;
   wire        S0_STALL_O, S1_STALL_O, S2_STALL_O,
	       S3_STALL_O, S4_STALL_O, S5_STALL_O;

   /* Non-wishbone */
   wire        M0_finished;
   wire        M1_finished;

   master_model
     #(
       .MASTER_NR(0),
       .WORD_SIZE(2),
       .ADR_BITS(20),
       .OPERATIONS_FILE("operations0.mem"),
       .OPERATIONS_COUNT(`MASTER0_OPERATIONS_COUNT)
       ) master0
       (
	.ACK_I(M0_ACK_I),
 	.CLK_I(CLK),
	.ADR_O(M0_ADR_O),
	.DAT_I(M0_DAT_I),
	.DAT_O(M0_DAT_O),
	.SEL_O(M0_SEL_O),
 	.RST_I(RST),
	.STB_O(M0_STB_O),
	.CYC_O(M0_CYC_O),
	.WE_O(M0_WE_O),
 	.STALL_I(M0_STALL_I),

	.finished(M0_finished)
	);

   master_model
     #(
       .MASTER_NR(1),
       .WORD_SIZE(2),
       .ADR_BITS(20),
       .OPERATIONS_FILE("operations1.mem"),
       .OPERATIONS_COUNT(`MASTER1_OPERATIONS_COUNT)
       ) master1
       (
	.ACK_I(M1_ACK_I),
 	.CLK_I(CLK),
	.ADR_O(M1_ADR_O),
	.DAT_I(M1_DAT_I),
	.DAT_O(M1_DAT_O),
	.SEL_O(M1_SEL_O),
 	.RST_I(RST),
	.STB_O(M1_STB_O),
	.CYC_O(M1_CYC_O),
	.WE_O(M1_WE_O),
 	.STALL_I(M1_STALL_I),

	.finished(M1_finished)
	);

   memory_slave_model
     #(
       .SLAVE_NR(0)
       ) slave0
       (
	.ACK_O(S0_ACK_O),
      	.CLK_I(CLK),
	.ADR_I(S0_ADR_I),
	.DAT_I(S0_DAT_I),
	.DAT_O(S0_DAT_O),
	.SEL_I(S0_SEL_I),
      	.RST_I(RST),
      	.STB_I(S0_STB_I),
      	.WE_I(S0_WE_I),
	.STALL_O(S0_STALL_O)
	);

   memory_slave_model
     #(
       .SLAVE_NR(1)
       ) slave1
       (
	.ACK_O(S1_ACK_O),
      	.CLK_I(CLK),
	.ADR_I(S1_ADR_I),
	.DAT_I(S1_DAT_I),
	.DAT_O(S1_DAT_O),
	.SEL_I(S1_SEL_I),
      	.RST_I(RST),
      	.STB_I(S1_STB_I),
      	.WE_I(S1_WE_I),
	.STALL_O(S1_STALL_O)
	);

   memory_slave_model
     #(
       .SLAVE_NR(2),
       .ADR_BITS(17)
       ) slave2
       (
	.ACK_O(S2_ACK_O),
      	.CLK_I(CLK),
	.ADR_I(S2_ADR_I),
	.DAT_I(S2_DAT_I),
	.DAT_O(S2_DAT_O),
	.SEL_I(S2_SEL_I),
      	.RST_I(RST),
      	.STB_I(S2_STB_I),
      	.WE_I(S2_WE_I),
	.STALL_O(S2_STALL_O)
	);

   memory_slave_model
     #(
       .SLAVE_NR(3),
       .ADR_BITS(17)
       ) slave3
       (
	.ACK_O(S3_ACK_O),
      	.CLK_I(CLK),
	.ADR_I(S3_ADR_I),
	.DAT_I(S3_DAT_I),
	.DAT_O(S3_DAT_O),
	.SEL_I(S3_SEL_I),
      	.RST_I(RST),
      	.STB_I(S3_STB_I),
      	.WE_I(S3_WE_I),
	.STALL_O(S3_STALL_O)
	);

   memory_slave_model
     #(
       .SLAVE_NR(4),
       .ADR_BITS(17)
       ) slave4
       (
	.ACK_O(S4_ACK_O),
      	.CLK_I(CLK),
	.ADR_I(S4_ADR_I),
	.DAT_I(S4_DAT_I),
	.DAT_O(S4_DAT_O),
	.SEL_I(S4_SEL_I),
      	.RST_I(RST),
      	.STB_I(S4_STB_I),
      	.WE_I(S4_WE_I),
	.STALL_O(S4_STALL_O)
	);

   memory_slave_model
     #(
       .SLAVE_NR(5),
       .ADR_BITS(17)
       ) slave5
       (
	.ACK_O(S5_ACK_O),
      	.CLK_I(CLK),
	.ADR_I(S5_ADR_I),
	.DAT_I(S5_DAT_I),
	.DAT_O(S5_DAT_O),
	.SEL_I(S5_SEL_I),
      	.RST_I(RST),
      	.STB_I(S5_STB_I),
      	.WE_I(S5_WE_I),
	.STALL_O(S5_STALL_O)
	);

   intercon intercon
     (
      .CLK(CLK),
      .RST(RST),

      .S0_ACK_O(S0_ACK_O),
      .S0_ADR_I(S0_ADR_I),
      .S0_DAT_I(S0_DAT_I),
      .S0_DAT_O(S0_DAT_O),
      .S0_STB_I(S0_STB_I),
      .S0_WE_I(S0_WE_I),
      .S0_STALL_O(S0_STALL_O),

      .S1_ACK_O(S1_ACK_O),
      .S1_ADR_I(S1_ADR_I),
      .S1_DAT_I(S1_DAT_I),
      .S1_DAT_O(S1_DAT_O),
      .S1_STB_I(S1_STB_I),
      .S1_WE_I(S1_WE_I),
      .S1_STALL_O(S1_STALL_O),

      .S2_ACK_O(S2_ACK_O),
      .S2_ADR_I(S2_ADR_I),
      .S2_DAT_I(S2_DAT_I),
      .S2_DAT_O(S2_DAT_O),
      .S2_STB_I(S2_STB_I),
      .S2_WE_I(S2_WE_I),
      .S2_STALL_O(S2_STALL_O),

      .S3_ACK_O(S3_ACK_O),
      .S3_ADR_I(S3_ADR_I),
      .S3_DAT_I(S3_DAT_I),
      .S3_DAT_O(S3_DAT_O),
      .S3_STB_I(S3_STB_I),
      .S3_WE_I(S3_WE_I),
      .S3_STALL_O(S3_STALL_O),

      .S4_ACK_O(S4_ACK_O),
      .S4_ADR_I(S4_ADR_I),
      .S4_DAT_I(S4_DAT_I),
      .S4_DAT_O(S4_DAT_O),
      .S4_STB_I(S4_STB_I),
      .S4_WE_I(S4_WE_I),
      .S4_STALL_O(S4_STALL_O),

      .S5_ACK_O(S5_ACK_O),
      .S5_ADR_I(S5_ADR_I),
      .S5_DAT_I(S5_DAT_I),
      .S5_DAT_O(S5_DAT_O),
      .S5_STB_I(S5_STB_I),
      .S5_WE_I(S5_WE_I),
      .S5_STALL_O(S5_STALL_O),

      .M0_ACK_I(M0_ACK_I),
      .M0_ADR_O(M0_ADR_O),
      .M0_DAT_I(M0_DAT_I),
      .M0_DAT_O(M0_DAT_O),
      .M0_STB_O(M0_STB_O),
      .M0_CYC_O(M0_CYC_O),
      .M0_WE_O(M0_WE_O),
      .M0_STALL_I(M0_STALL_I),

      .M1_ACK_I(M1_ACK_I),
      .M1_ADR_O(M1_ADR_O),
      .M1_DAT_I(M1_DAT_I),
      .M1_DAT_O(M1_DAT_O),
      .M1_STB_O(M1_STB_O),
      .M1_CYC_O(M1_CYC_O),
      .M1_WE_O(M1_WE_O),
      .M1_STALL_I(M1_STALL_I)
      );

   assign S0_SEL_I = 1;
   assign S1_SEL_I = 1;
   assign S2_SEL_I = 1;
   assign S3_SEL_I = 1;
   assign S4_SEL_I = 1;
   assign S5_SEL_I = 1;

   integer     i;

   initial begin
      CLK <= 0;
      RST <= 1;

      for (i = 0; i < 1000; i++) begin
	 #1;

	 CLK <= ~CLK;

	 if (CLK)
	   RST <= 0;

	 if (M0_finished && M1_finished)
	   $finish;
      end

      if (!M0_finished)
	$display("error: master 0 hasn't finished its operations in 500 ticks");

      if (!M1_finished)
	$display("error: master 1 hasn't finished its operations in 500 ticks");

      $finish;
   end
endmodule // intercon_test
