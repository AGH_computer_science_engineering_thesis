;;; this test is based on wasm_compile_if_else
(module
 (memory 0 2)
 (func $eq10 (param $x i32) (result i32)
       (if (result i32)
	   (i32.sub
	    (get_local $x)
	    (i32.const 10))
	 (then
	  (br 1 (i32.add (i32.const -3) (i32.const 3)))
	  ;; unreachable instruction
	  ;; (i32.const 5)
	  )
	 (else
	  (i32.const -1))))
 (func $main
       ;; write 0x00000000 at MEMORY_BOTTOM_ADDR
       (i32.store offset=0x0 align=4 (i32.const 0x0)
		  (call $eq10 (i32.const 12)))
       ;; write 0xFFFFFFFF at MEMORY_BOTTOM_ADDR + 0x4
       (i32.store offset=0x0 align=4 (i32.const 0x4)
		  (call $eq10 (i32.const 10))))
 (export "main" (func $main)))
