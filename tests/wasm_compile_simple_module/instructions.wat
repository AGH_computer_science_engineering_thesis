(module
 (memory 0 2)
 (func $main
       i32.const 0x1D ;; dynamic offset for memory store instruction
       i32.const 0x1E ;; value to store
       i32.store offset=0x1F align=4) ;; we store the difference at 0x23C
 (export "main" (func $main)))
