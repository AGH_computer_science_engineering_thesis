(module
 (memory 0 2)
 (func $main
       (local $counter i32)

       ;; prepare offset for store operation later
       (i32.const 0x0)

       ;; initialize counter
       (set_local $counter (i32.const 0))

       ;; add numbers from 1 to 10
       (i32.const 0)
       (loop $again (param i32) (result i32)
	     (set_local $counter (i32.add
				  (get_local $counter)
				  (i32.const 1)))
	     (i32.add (get_local $counter))
	     (br_if $again (i32.lt_u
			    (get_local $counter)
			    (i32.const 10))))

       ;; write computed sum (55 in dec, 37 in hex) at MEMORY_BOTTOM_ADDR
       (i32.store offset=0x0 align=4))
 (export "main" (func $main)))
