;;; this test is loosely based on wasm_compile_br
(module
 (memory 0 1)
 ;; this function returns -1 if its argument is 10 or 11 and 0 otherwise
 (func $eq10or11 (param $x i32) (result i32)
       (if (result i32)
	   (i32.sub
	    (get_local $x)
	    (i32.const 10))
	 (then
	  (br_if 0 (i32.add (i32.const -3) (i32.const 3))
		 (i32.sub (get_local $x) (i32.const 11)))
	  (i32.const 1)
	  (i32.sub))
	 (else
	  (i32.const -1))))
 (func $main
       ;; write 0x00000000 at MEMORY_BOTTOM_ADDR
       (i32.store offset=0x0 align=4 (i32.const 0x0)
		  (call $eq10or11 (i32.const 12)))
       ;; write 0xFFFFFFFF at MEMORY_BOTTOM_ADDR + 0x4
       (i32.store offset=0x0 align=4 (i32.const 0x4)
		  (call $eq10or11 (i32.const 10)))
       ;; write 0xFFFFFFFF at MEMORY_BOTTOM_ADDR + 0x8
       (i32.store offset=0x0 align=4 (i32.const 0x8)
		  (call $eq10or11 (i32.const 11))))
 (export "main" (func $main)))
