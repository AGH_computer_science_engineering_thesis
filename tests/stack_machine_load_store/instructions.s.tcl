### set stack to 0, store 4 numbers (h2B, h3B, h4B and h5B) at addresses
### h000002, h000012, h000022 and h000032 and load them back to stack (only 2
### will really get written to stack's memory, other 2 will remain in r0 and
### r1). Then, write them to h000008, h000018, h000028 and h000038.
### At the and use operand-addressing to load back value h3B from h000018
### (expressed as h000017 in im + h000001 in operand) and store it to h1FFFE2
### (expressed as h1FFFC0 in im + h000022 in operand).

### we're using small (in absolute) numbers and addresses for simplicity

## set up stack
_set_sp 0

## get number h2B into r1, and store it - repeat for other 3 numbers;
_const h2B
_store h02

_const h3B
_store h12

_const h2A
_store h22

_const h3A
_store h32

## get numbers back on stack
_load h02
_load h12
_load h22
_load h32

## store copies to new locations
_store h38
_store h28
_store h18
_store h08

## prepare operand for later store operation
_const h22

## use operand addressing to load back value from h000018
_const h01
_load+ h17

## use operand addressing to trove value to h1FFFE2 = h1FFFC0 + h000022
# immediate value gets sign-extended here!
_store+ h40

## finish test
halt
