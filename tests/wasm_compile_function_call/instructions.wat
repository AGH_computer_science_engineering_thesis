(module
 (memory 0 2)
 (func $main
       i32.const 0x17 ;; dynamic offset for memory store instruction
       i32.const 0x32 ;; value to substract from
       i32.const 0x14 ;; value to substract
       call $sub
       i32.store offset=0x25 align=4) ;; we store the difference at 0x23C
 (func $sub (param $lhs i32) (param $rhs i32) (result i32)
       local.get $lhs
       local.get $rhs
       i32.sub)
 (export "main" (func $main)))
