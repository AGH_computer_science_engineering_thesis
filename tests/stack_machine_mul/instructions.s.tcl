### store 4 values to memory; load 2 of them back, multiply them and store the
### result; load another 2, multiply them and store the result;
### this is similar to addition and substraction tests

set_sp 0

## store first 2 factors
const 483091365
store h1EEE0
const 74683203
store h1EEE4

## store other 2 factors
const 8436
store h1EEEC
const -14020
store h1EEF0

## perform the first multiplication
load h1EEE0
load h1EEE4
# multiplying 483091365 by 74683203 should yield 36078810479842095
# if we take lowest 32 bits of 36078810479842095 we get 2861683503
mul
store h1EEE8

## perform the second multiplication
load h1EEEC
load h1EEF0
# multiplying 8436 by -14020 should yield -118272720 (which fits in 32 bits)
mul
store h1EEF4

halt
