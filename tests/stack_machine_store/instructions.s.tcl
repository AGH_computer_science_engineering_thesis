### simple test - write value hDEADBEEF to address h3ABCD;
### then, write value h900DDEED to address FFBCE, but use operand addressing
### (i.e. with hFFBBD in im and h00011 in r0)

## get address h0 into sp
_set_sp 0

## get value hDEADBEEF into r1
#                             DDDD EEEE AA
# bits 31:22 of hDEADBEEF are 1101 1110 10
_im b1101111010
#                            AA DDDD BBBB EEEE E
# bits 21:7 of hDEADBEEF are 10 1101 1011 1110 1
_im b101101101111101
#                           EEE FFFF
# bits 6:0 of hDEADBEEF are 110 1111
_const b1101111


## get address h3ABCD into im and write to memory
#                         33 AAAA BBBB C
# bits 17:7 of h3ABCD are 11 1010 1011 1
_im b11101010111
#                        CCC DDDD
# bits 6:0 of h3ABCD are 100 1101
_store b1001101


## get value h00011 into r1
#                        111 1111
# bits 6:0 of h00011 are 001 0001
_const b0010001

## get value h900DDEED into r1
#                             9999 0000 00
# bits 31:22 of h900DDEED are 1001 0000 00
_im b1001000000
#                            00 DDDD DDDD EEEE E
# bits 21:7 of h900DDEED are 00 1101 1101 1110 1
_im b001101110111101
#                           EEE DDDD
# bits 6:0 of h900DDEED are 110 1101
_const b1101101

## get address hFFBBD into im and write to memory
#                         FFFF FFFF BBBB B
# bits 19:7 of hFFBBD are 1111 1111 1011 1
_im b1111111110111
#                        BBB DDDD
# bits 6:0 of hFFBBD are 011 1101
_store+ b0111101

## finish test
halt
