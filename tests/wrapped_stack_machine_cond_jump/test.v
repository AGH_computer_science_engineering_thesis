`default_nettype none

`include "messages.vh"

`ifndef SIMULATION
 `error_SIMULATION_not_defined
; /* Cause syntax error */
`endif

`ifndef INSTRUCTIONS_COUNT
 `error_INSTRUCTIONS_COUNT_must_be_defined
; /* Cause syntax error */
`endif

`ifndef WORDS_TO_VERIFY_COUNT
 `error_WORDS_TO_VERIFY_COUNT_must_be_defined
; /* Cause syntax error */
`endif

module wrapped_stack_machine_test();
   reg 	       CLK;
   reg 	       RST;

   wire        MI_ACK_I;
   wire [19:0] MI_ADR_O;
   wire [15:0] MI_DAT_I;
   wire [15:0] MI_DAT_O;
   wire        MI_STB_O;
   wire        MI_CYC_O;
   wire        MI_WE_O;
   wire        MI_STALL_I;

   wire        MD_ACK_I;
   wire [19:0] MD_ADR_O;
   wire [15:0] MD_DAT_I;
   wire [15:0] MD_DAT_O;
   wire        MD_STB_O;
   wire        MD_CYC_O;
   wire        MD_WE_O;
   wire        MD_STALL_I;

   /* For simple tests we'll use separate slaves for instructions and data */
   wire        SI_ACK_O;
   wire [19:0] SI_ADR_I;
   wire [15:0] SI_DAT_I;
   wire [15:0] SI_DAT_O;
   wire        SI_SEL_I;
   wire        SI_STB_I;
   wire        SI_WE_I;
   wire        SI_STALL_O;

   wire        SD_ACK_O;
   wire [19:0] SD_ADR_I;
   wire [15:0] SD_DAT_I;
   wire [15:0] SD_DAT_O;
   wire        SD_SEL_I;
   wire        SD_STB_I;
   wire        SD_WE_I;
   wire        SD_STALL_O;

   /* Non-wishbone */
   wire        M_finished;

   wrapped_stack_machine stack_machine
     (
      .CLK_I(CLK),
      .RST_I(RST),

      /* Instruction reading interface */
      .I_ACK_I(MI_ACK_I),
      .I_ADR_O(MI_ADR_O),
      .I_DAT_I(MI_DAT_I),
      .I_DAT_O(MI_DAT_O),
      .I_STB_O(MI_STB_O),
      .I_CYC_O(MI_CYC_O),
      .I_WE_O(MI_WE_O),
      .I_STALL_I(MI_STALL_I),

      /* Data interface */
      .D_ACK_I(MD_ACK_I),
      .D_ADR_O(MD_ADR_O),
      .D_DAT_I(MD_DAT_I),
      .D_DAT_O(MD_DAT_O),
      .D_STB_O(MD_STB_O),
      .D_CYC_O(MD_CYC_O),
      .D_WE_O(MD_WE_O),
      .D_STALL_I(MD_STALL_I),

      .finished(M_finished)
      );

   memory_slave_model
     #(
       .SLAVE_NR(0),
       .WORD_SIZE(2),
       .ADR_BITS(20),
       .WRITABLE(0),
       .WORDS_TO_INITIALIZE(`INSTRUCTIONS_COUNT),
       .INITIAL_CONTENTS_FILE("instructions.mem")
       ) slave_I
       (
	.ACK_O(SI_ACK_O),
      	.CLK_I(CLK),
	.ADR_I(SI_ADR_I),
	.DAT_I(SI_DAT_I),
	.DAT_O(SI_DAT_O),
	.SEL_I(SI_SEL_I),
      	.RST_I(RST),
      	.STB_I(SI_STB_I),
      	.WE_I(SI_WE_I),
	.STALL_O(SI_STALL_O)
	);

   memory_slave_model
     #(
       .SLAVE_NR(1),
       .WORD_SIZE(2),
       .ADR_BITS(20),
       .WRITABLE(1)
       ) slave_D
       (
	.ACK_O(SD_ACK_O),
      	.CLK_I(CLK),
	.ADR_I(SD_ADR_I),
	.DAT_I(SD_DAT_I),
	.DAT_O(SD_DAT_O),
	.SEL_I(SD_SEL_I),
      	.RST_I(RST),
      	.STB_I(SD_STB_I),
      	.WE_I(SD_WE_I),
	.STALL_O(SD_STALL_O)
	);

   assign MI_ACK_I = SI_ACK_O;
   assign MI_DAT_I = SI_DAT_O;
   assign MI_STALL_I = SI_STALL_O;

   assign MD_ACK_I = SD_ACK_O;
   assign MD_DAT_I = SD_DAT_O;
   assign MD_STALL_I = SD_STALL_O;

   assign SI_ADR_I = MI_ADR_O;
   assign SI_DAT_I = MI_DAT_O;
   assign SI_SEL_I = 1;
   assign SI_STB_I = MI_STB_O && MI_CYC_O;
   assign SI_WE_I = MI_WE_O;

   assign SD_ADR_I = MD_ADR_O;
   assign SD_DAT_I = MD_DAT_O;
   assign SD_SEL_I = 1;
   assign SD_STB_I = MD_STB_O && MD_CYC_O;
   assign SD_WE_I = MD_WE_O;

   integer     i, j;
   reg [19:0]  address;
   reg [15:0]  expected_value;

   reg [20:0]  words_to_verify[`WORDS_TO_VERIFY_COUNT * 2 - 1 : 0];

   initial begin
      CLK <= 0;
      RST <= 1;

      for (i = 0; i < 5000; i++) begin
	 #1;

	 CLK <= ~CLK;

	 if (CLK)
	   RST <= 0;

	 if (M_finished) begin
	    $readmemh("words_to_verify.mem", words_to_verify,
		      0, `WORDS_TO_VERIFY_COUNT * 2 - 1);

	    for (j = 0; j < `WORDS_TO_VERIFY_COUNT; j++) begin
	       /* Keep in mind we haven't implemented byte-grained access yet */
	       address = words_to_verify[2 * j][20:1];
	       expected_value = words_to_verify[2 * j + 1];
	       if (slave_D.memory[address] !== expected_value) begin
		  `MSG(("error: expected h%x at h%x, but got h%x",
			expected_value, address, slave_D.memory[address]));
	       end
	    end

	    $finish;
	 end // if (M_finished)
      end // for (i = 0; i < 5000; i++)

      $display("error: cpu hasn't finished its operations in 2500 ticks");
      $finish;
   end // initial begin
endmodule // wrapped_stack_machine_test
