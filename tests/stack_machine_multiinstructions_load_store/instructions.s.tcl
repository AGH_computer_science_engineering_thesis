### set stack to 0, store 4 numbers (h11223344, h55667788, h8899AABB and
### hCCDDEEFF) at addresses h00002E, h00003E, h00004E and h00005E and load them
### back to stack (only 2 will really get written to stack's memory, other 2
### will remain in r0 and r1). Then, write them to h00F0F0, h08F0F0, h10F0F0 and
### h18F0F0.
### At the and use operand-addressing to load back value h55667788 from h00003E
### (expressed as h000040 in im + (-2) in operand) and store it to h1FFFCD
### (expressed as h1F0F0D in im + h00F0C0 in operand).

## set up stack
set_sp 0

## get number h11223344 into r1, and store it - repeat for other 3 numbers;
const h11223344
store h00002E

const h55667788
store h00003E

const h8899AABB
store h00004E

const hCCDDEEFF
store h00005E

## get numbers back on stack
load h00002E
load h00003E
load h00004E
load h00005E

## store copies to new locations
store h18F0F0
store h10F0F0
store h08F0F0
store h00F0F0

## prepare operand for later store operation
const h00F0C0

## use operand addressing to load back value from h00003E
const -2
load+ h000040

## use operand addressing to store value to h1FFFCD = h1F0F0D + h00F0C0
store+ h1F0F0D

## finish test
halt
