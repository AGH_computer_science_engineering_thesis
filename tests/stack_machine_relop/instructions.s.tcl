### store 2 values to memory, load them back, add them and store the result

set_sp 0

## compare 1234567890 to itself

const 1234567890
const 1234567890
# yields 1
eq
store h1EEE0

const 1234567890
const 1234567890
# yields 0
lt
store h1EEE4

const 1234567890
const 1234567890
# yields 0
ult
store h1EEE8

const 1234567890
const 1234567890
# yields 1
le
store h1EEEC

const 1234567890
const 1234567890
# yields 1
ule
store h1EEF0

const 1234567890
const 1234567890
# yields 0
gt
store h1EEF4

const 1234567890
const 1234567890
# yields 0
ugt
store h1EEF8

const 1234567890
const 1234567890
# yields 1
ge
store h1EEFC

const 1234567890
const 1234567890
# yields 1
uge
store h1EF00


## compare hFFFFFFFF to h7FFFFFFF
## hFFFFFFFF will be treated as negative for signed operations

const hFFFFFFFF
const h7FFFFFFF
# yields 0
eq
store h1F0E0

const hFFFFFFFF
const h7FFFFFFF
# yields 1
lt
store h1F0E4

const hFFFFFFFF
const h7FFFFFFF
# yields 0
ult
store h1F0E8

const hFFFFFFFF
const h7FFFFFFF
# yields 1
le
store h1F0EC

const hFFFFFFFF
const h7FFFFFFF
# yields 0
ule
store h1F0F0

const hFFFFFFFF
const h7FFFFFFF
# yields 0
gt
store h1F0F4

const hFFFFFFFF
const h7FFFFFFF
# yields 1
ugt
store h1F0F8

const hFFFFFFFF
const h7FFFFFFF
# yields 0
ge
store h1F0FC

const hFFFFFFFF
const h7FFFFFFF
# yields 1
uge
store h1F100


## compare 18532 to 234, no signedness magic here

const 18532
const 234
# yields 0
eq
store h1F2E0

const 18532
const 234
# yields 0
lt
store h1F2E4

const 18532
const 234
# yields 0
ult
store h1F2E8

const 18532
const 234
# yields 0
le
store h1F2EC

const 18532
const 234
# yields 0
ule
store h1F2F0

const 18532
const 234
# yields 1
gt
store h1F2F4

const 18532
const 234
# yields 1
ugt
store h1F2F8

const 18532
const 234
# yields 1
ge
store h1F2FC

const 18532
const 234
# yields 1
uge
store h1F300


## compare 123 to -1294081
## -1294081 will be interpreted as big positive number for unsigned operations

const 123
const -1294081
# yields 0
eq
store h1F4E0

const 123
const -1294081
# yields 0
lt
store h1F4E4

const 123
const -1294081
# yields 1
ult
store h1F4E8

const 123
const -1294081
# yields 0
le
store h1F4EC

const 123
const -1294081
# yields 1
ule
store h1F4F0

const 123
const -1294081
# yields 1
gt
store h1F4F4

const 123
const -1294081
# yields 0
ugt
store h1F4F8

const 123
const -1294081
# yields 1
ge
store h1F4FC

const 123
const -1294081
# yields 0
uge
store h1F500


halt
