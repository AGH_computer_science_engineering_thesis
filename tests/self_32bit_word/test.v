/* adapted from self test */
`default_nettype none

`include "messages.vh"

`ifndef MASTER_OPERATIONS_COUNT
 `error_MASTER_OPERATIONS_COUNT_must_be_defined
; /* Cause syntax error */
`endif

`ifndef SIMULATION
 `error_SIMULATION_not_defined
; /* Cause syntax error */
`endif

module self_32bit_test();
   wire        M_ACK_I;
   wire        M_CLK_I;
   wire [21:0] M_ADR_O;
   wire [31:0] M_DAT_I;
   wire [31:0] M_DAT_O;
   wire [3:0]  M_SEL_O;
   wire        M_RST_I;
   wire        M_STB_O;
   wire        M_CYC_O;
   wire        M_WE_O;
   wire        M_STALL_I;

   wire        S_ACK_O;
   wire        S_CLK_I;
   wire [21:0] S_ADR_I;
   wire [31:0] S_DAT_I;
   wire [31:0] S_DAT_O;
   wire [3:0]  S_SEL_I;
   wire        S_RST_I;
   wire        S_STB_I;
   wire        S_WE_I;
   wire        S_STALL_O;

   /* Non-wishbone */
   wire        M_finished;

   master_model
     #(
       .MASTER_NR(0),
       .WORD_SIZE(4),
       .SEL_LINES(4),
       .ADR_BITS(22),
       .OPERATIONS_FILE("operations.mem"),
       .OPERATIONS_COUNT(`MASTER_OPERATIONS_COUNT)
       ) master
       (
	.ACK_I(M_ACK_I),
 	.CLK_I(M_CLK_I),
	.ADR_O(M_ADR_O),
	.DAT_I(M_DAT_I),
	.DAT_O(M_DAT_O),
	.SEL_O(M_SEL_O),
 	.RST_I(M_RST_I),
	.STB_O(M_STB_O),
	.CYC_O(M_CYC_O),
	.WE_O(M_WE_O),
 	.STALL_I(M_STALL_I),

	.finished(M_finished)
	);

   memory_slave_model
     #(
       .SLAVE_NR(0),
       .WORD_SIZE(4),
       .SEL_LINES(4),
       .ADR_BITS(22)
       ) slave
       (
	.ACK_O(S_ACK_O),
      	.CLK_I(S_CLK_I),
	.ADR_I(S_ADR_I),
	.DAT_I(S_DAT_I),
	.DAT_O(S_DAT_O),
	.SEL_I(S_SEL_I),
      	.RST_I(S_RST_I),
      	.STB_I(S_STB_I),
      	.WE_I(S_WE_I),
	.STALL_O(S_STALL_O)
	);

   reg 	       CLK;
   reg 	       RST;

   assign M_ACK_I = S_ACK_O;
   assign M_CLK_I = CLK;
   assign M_DAT_I = S_DAT_O;
   assign M_RST_I = RST;
   assign M_STALL_I = S_STALL_O;

   assign S_CLK_I = CLK;
   assign S_ADR_I = M_ADR_O;
   assign S_DAT_I = M_DAT_O;
   assign S_SEL_I = M_SEL_O;
   assign S_RST_I = RST;
   assign S_STB_I = M_STB_O && M_CYC_O;
   assign S_WE_I = M_WE_O;

   integer     i;

   initial begin
      CLK <= 0;
      RST <= 1;

      for (i = 0; i < 600; i++) begin
	 #1;

	 CLK <= ~CLK;

	 if (CLK)
	   RST <= 0;

	 if (M_finished)
	   $finish;
      end

      $display("error: master hasn't finished its operations in 300 ticks");
      $finish;
   end
endmodule // self_32bit_test
