## Let there be 2 pieces of code - each one storing a different value in memory
## and halting afterwards. Jump instruction should cause the second piece of
## code to execute instead of the first. We'll verify that by checking, which
## value got written.

## Ufortunately, our assembly currently doesn't compute label addresses for us.

# Those 4 instructions should get encoded into 5 16-bit words, so they
# will take 10 bytes total. Hence jump to address 10 is needed to skip them.
jump 10
const h12
store h87C0
halt

# address 10 here
const h34
store h87C0
halt
