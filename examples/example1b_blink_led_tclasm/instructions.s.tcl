## adapted from soc_measure_time test

## we're going to write numbers from 0 to 639 at addresses h100000 to h1009FC
## and then write non-zero value at h100A00

# this will translate to 2 16-bit instructions
set_sp h100000

## load current value of timer, in a loop
## this is address 4 we later jump to
# this will translate to 2 16-bit instructions
loadwzx h1C0008

## loop until timer exceeds 1500
# this will translate to 2 16-bit instructions
const 1500
# this will translate to 1 16-bit instruction
lt
# this will translate to 1 16-bit instruction
cond_jump 4

## now, light led2
# this will translate to 1 16-bit instruction
const 1
# this will translate to 2 16-bit instructions
storew h1C0006

## second loop, analogous to the first one
## this is address 22 we later jump to
# this will translate to 2 16-bit instructions
loadwzx h1C0008

## loop until timer exceeds 3000
# this will translate to 2 16-bit instructions
const 3000
# this will translate to 1 16-bit instruction
lt
# this will translate to 1 16-bit instruction
cond_jump 22

## now, switch led2 off
# this will translate to 1 16-bit instruction
const 0
# this will translate to 2 16-bit instructions
storew h1C0006

## third loop, analogous to the first two
## this is address 40 we later jump to
loadwzx h1C0008

## loop until timer exceeds 4500
const 4500
lt
cond_jump 40

## finish operation (will also put led1 on)
halt
