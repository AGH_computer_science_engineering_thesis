`default_nettype none
`timescale 1ns/1ns

`include "messages.vh"

`ifndef SIMULATION
 `error_SIMULATION_not_defined
; /* Cause syntax error */
`endif

`ifndef ROM_WORDS_COUNT
 `error_ROM_WORDS_COUNT_must_be_defined
; /* Cause syntax error */
`endif

`ifndef EMBEDDED_ROM_FILE
 `error_EMBEDDED_ROM_FILE_not_defined
; /* Cause syntax error */
`endif

`ifndef FONT_FILE
 `error_FONT_FILE_not_defined
; /* Cause syntax error */
`endif

`ifndef SPI_ROM_FILE
 `define SPI_ROM_FILE "/dev/null"
 `define SPI_ROM_WORDS_COUNT 0
`else
 `ifndef SPI_ROM_WORDS_COUNT
  `error_SPI_ROM_WORDS_COUNT_not_defined
; /* Cause syntax error */
 `endif
`endif

`ifndef FINISH_ON_IMAGE_WRITES
 `define FINISH_ON_IMAGE_WRITES 0
`else
 `define FINISH_RULE_PROVIDED 1
`endif

`ifndef FINISH_ON_LED1
 `define FINISH_ON_LED1 -1
`else
 `define FINISH_RULE_PROVIDED 1
`endif

`ifndef FINISH_ON_LED2
 `define FINISH_ON_LED2 -1
`else
 `define FINISH_RULE_PROVIDED 1
`endif

`ifndef FINISH_RULE_PROVIDED
 `define FINISH_RULE_PROVIDED 0
`endif

`ifndef MIN_TIME_NS
 `define MIN_TIME_NS 1000
`endif

`ifndef MAX_TIME_NS
 `define MAX_TIME_NS 250_000_000
`endif

module example();
   wire [9:0] image_writes;
   wire       can_finish;

   reg 	      clock_100mhz;
   reg 	      reset;

   wire       led1;
   wire       led2;

   soc_with_peripherals
     #(
       .FONT_FILE(`FONT_FILE),
       .EMBEDDED_ROM_WORDS_COUNT(`ROM_WORDS_COUNT),
       .EMBEDDED_ROM_FILE(`EMBEDDED_ROM_FILE),
       .SPI_ROM_WORDS_COUNT(`SPI_ROM_WORDS_COUNT),
       .SPI_ROM_FILE(`SPI_ROM_FILE)
       ) soc
       (
	.clock_100mhz(clock_100mhz),

	.button1(!reset),
	.button2(1'b1),

	.led1(led1),
	.led2(led2),

	.image_writes(image_writes)
	);

   integer    i;

   initial begin
      reset <= 1;
      clock_100mhz <= 0;

      if (!`FINISH_RULE_PROVIDED) begin
	 `MSG(("No finish rule provided, simulation will terminate after %0d ns,",
	       `MAX_TIME_NS));
      end

      for (i = 0; i < `MAX_TIME_NS; i = i + 5) begin
	 #5;

	 if (clock_100mhz)
	   reset <= 0;

	 clock_100mhz <= ~clock_100mhz;
      end

      `MSG(("%0d ns passed, finishing", `MAX_TIME_NS));
      $finish;
   end

   assign can_finish = i >= `MIN_TIME_NS;

   always @ (led1) begin
      if (i > 0) begin
	 if (!led1)
	   `MSG(("LED1 goes on, stack machine finished operation"));
	 else
	   `MSG(("LED1 goes off, stack machine resets"));
      end
   end

   always @ (led2) begin
      if (i > 35) begin
	 if (!led2)
	   `MSG(("LED2 goes on"));
	 else
	   `MSG(("LED2 goes off"));
      end
   end

   always @ (image_writes) begin
      if (image_writes)
	`MSG(("Display refreshed %0d time(s)", image_writes));
   end

   generate
      if (`FINISH_RULE_PROVIDED) begin
	 always @ (image_writes or led1 or led2 or can_finish) begin
	    if ((image_writes >= `FINISH_ON_IMAGE_WRITES) &&
		(`FINISH_ON_LED1 < 0 || `FINISH_ON_LED1 == !led1) &&
		(`FINISH_ON_LED2 < 0 || `FINISH_ON_LED2 == !led2) &&
		can_finish) begin
	       #1;
	       `MSG(("finishing"));
	       $finish;
	    end
	 end
      end // if (`FINISH_RULE_PROVIDED)
   endgenerate
endmodule // example
