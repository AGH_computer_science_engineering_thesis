;; Relevant addresses are lower half of timer reg (0x1BFC08)
;; and led2 reg (0x1BFC06).

(module
 (memory 0 2)
 (func $main
       ;; loop until timer exceeds 1500
       (loop $again
	     (br_if $again (i32.lt_u
			    (i32.load16_u offset=0x0 align=2
					  (i32.const 0x1BFC08))
			    (i32.const 1500))))

       ;; now, light led2
       (i32.store16 offset=0x0 align=2
		    (i32.const 0x1BFC06) (i32.const 0x1))

       ;; loop until timer exceeds 3000
       (loop $again
	     (br_if $again (i32.lt_u
			    (i32.load16_u offset=0x0 align=2
					  (i32.const 0x1BFC08))
			    (i32.const 3000))))

       ;; now, switch led2 off
       (i32.store16 offset=0x0 align=2
		    (i32.const 0x1BFC06) (i32.const 0x0))

       ;; loop until timer exceeds 4500
       (loop $again
	     (br_if $again (i32.lt_u
			    (i32.load16_u offset=0x0 align=2
					  (i32.const 0x1BFC08))
			    (i32.const 4500)))))
 (export "main" (func $main)))
