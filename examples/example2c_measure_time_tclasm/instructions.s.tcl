## also look at soc_measure_time test

### we do some of the same operations as in example2a and example2b

# this will translate to 2 16-bit instructions
set_sp h100000

## reset the timer
# this will translate to 1 16-bit instruction
const 0
# this will translate to 2 16-bit instructions
storew h1C0008

### we're going to add numbers from 0 to 100
## initialize counter
# this will translate to 1 16-bit instruction
const 0
# this will translate to 2 16-bit instructions
storew h080000

## initialize sum variable
# this will translate to 1 16-bit instruction
const 0

### this is the point we later jump to, address 18
## load counter value, add it to the sum
loadwzx h080000
add

## increase counter
loadwzx h080000
const 1
add
storew h080000

## compare counter to 100
loadwzx h080000
const 100
lt
cond_jump 18

## light LED2
const 1
storew h1C0006

### at this point we skip displaying to VGA - it'd be too cumbersome
### to do in tclasm
## store sum somewhere
store h080004

## finish operation (will also put led1 on)
halt
