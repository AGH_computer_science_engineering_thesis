`default_nettype none
`timescale 1ns/1ns

`include "messages.vh"

`ifndef SIMULATION
 `error_SIMULATION_not_defined
; /* Cause syntax error */
`endif

module K6R4016V1D_TC10_sram
  (
   input wire [17:0] sram_addr,
   inout wire [15:0] sram_io,

   input wire 	     sram_cs_not,
   input wire 	     sram_oe_not,
   input wire 	     sram_we_not
   );

   reg [15:0] 	     memory [2 ** 18 - 1 : 0];

   wire 	     enabled;
   wire 	     read;
   wire 	     write;

   assign enabled = !sram_cs_not;
   assign read = enabled && !sram_oe_not && sram_we_not;
   assign write = enabled && !sram_we_not;

   integer 	     enabled_time;
   integer 	     read_time;
   integer 	     write_time;

   reg [17:0] 	     addr_last;

   wire 	     addr_unchanged;
   assign addr_unchanged = addr_last == sram_addr;
   integer 	     addr_unchanged_time;

   initial begin
      enabled_time <= 0;
      read_time <= 0;
      write_time <= 0;

      addr_unchanged_time <= 0;
   end

   reg [15:0] 	     output_data;
   reg 		     outputting;
   assign sram_io = outputting ? output_data : 16'hZZZZ;

   always #1 begin
      enabled_time <= enabled ? enabled_time + 1 : 0;
      write_time <= write ? write_time + 1 : 0;
      read_time <= read ? read_time + 1 : 0;

      addr_last <= sram_addr;

      if (!read && read_time > 0 && read_time < 8)
	`MSG(("SRAM: error: output enable signal active for only %dns",
	      read_time));

      if (!write && write_time > 0 && write_time < 9)
	`MSG(("SRAM: error: write enable signal active for only %dns",
	      write_time));

      if (((read && read_time) || (write && write_time)) && addr_unchanged)
	addr_unchanged_time <= addr_unchanged_time + 1;
      else
	addr_unchanged_time <= 0;

      if (write && write_time) begin
	 if (addr_unchanged) begin
	    if (write_time >= 9 && addr_unchanged_time >= 8) begin
	       memory[sram_addr] <= sram_io;

	       if (write_time == 9)
		 `DBG(("SRAM: write of h%x at h%x", sram_io, sram_addr));
	    end
	 end else begin
	    `MSG(("SRAM: error: address changed during write"));
	 end
      end

      if (read && read_time >= 8) begin
	 outputting <= 1;

	 if (addr_unchanged_time >= 7) begin
	    output_data <= memory[sram_addr];

	    if (addr_unchanged_time == 7)
	      `DBG(("SRAM: read of h%x at h%x", memory[sram_addr], sram_addr));
	 end else begin
	    output_data <= 16'hXXXX;
	 end
      end else begin
	 outputting <= 0;
      end
   end // always #1
endmodule // K6R4016V1D_TC10_sram
