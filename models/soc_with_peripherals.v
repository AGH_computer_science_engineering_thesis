`default_nettype none

`include "messages.vh"

`ifndef SIMULATION
 `error_SIMULATION_not_defined
; /* Cause syntax error */
`endif

module soc_with_peripherals
  #(
    parameter DESIGN_DIR = "../../design/",
    parameter FONT_FILE = {DESIGN_DIR, "font.mem"},
    parameter EMBEDDED_ROM_WORDS_COUNT = "error, rom words count not given",
    parameter EMBEDDED_ROM_FILE = {DESIGN_DIR, "rom.mem"},
    parameter SPI_ROM_WORDS_COUNT = 0,
    parameter SPI_ROM_FILE = "/dev/null"
    )
   (
    input wire 	      clock_100mhz,

    input wire 	      button1,
    input wire 	      button2,

    output wire       led1,
    output wire       led2,

    output wire [9:0] image_writes
    );

   wire [17:0] 	      sram_addr;
   wire [15:0] 	      sram_io;
   wire 	      sram_cs_n;
   wire 	      sram_oe_n;
   wire 	      sram_we_n;

   wire 	      vga_hs;
   wire 	      vga_vs;
   wire [2:0] 	      vga_red;
   wire [2:0] 	      vga_green;
   wire [2:0] 	      vga_blue;

   wire 	      spi_sdo;
   wire 	      spi_sdi;
   wire 	      spi_sck;
   wire 	      spi_ss_n;

   W25Q16BV_flash
     #(
       .BYTES_TO_INITIALIZE(SPI_ROM_WORDS_COUNT * 2),
       .INITIAL_CONTENTS_FILE(SPI_ROM_FILE)
       ) flash
       (
	.sdo(spi_sdo),
	.sdi(spi_sdi),
	.sck(spi_sck),
	.ss_n(spi_ss_n)
	);

   VGA_640_480_60Hz vga_display
     (
      .horizontal_sync(vga_hs),
      .vertical_sync(vga_vs),

      .red(vga_red),
      .green(vga_green),
      .blue(vga_blue),

      .image_writes(image_writes)
      );

   K6R4016V1D_TC10_sram sram
     (
      .sram_addr(sram_addr),
      .sram_io(sram_io),
      .sram_cs_not(sram_cs_n),
      .sram_oe_not(sram_oe_n),
      .sram_we_not(sram_we_n)
      );

   soc
     #(
       .FONT_FILE(FONT_FILE),
       .ROM_WORDS_COUNT(EMBEDDED_ROM_WORDS_COUNT),
       .ROM_FILE(EMBEDDED_ROM_FILE)
       ) soc
       (
	.clock_100mhz(clock_100mhz),

	.sram_addr(sram_addr),
	.sram_io(sram_io),

	.sram_cs_n(sram_cs_n),
	.sram_oe_n(sram_oe_n),
	.sram_we_n(sram_we_n),

	.vga_hs(vga_hs),
	.vga_vs(vga_vs),
	.vga_red(vga_red),
	.vga_green(vga_green),
	.vga_blue(vga_blue),

	.spi_sdo(spi_sdo),
	.spi_sdi(spi_sdi),
	.spi_sck(spi_sck),
	.spi_ss_n(spi_ss_n),

	.button1(button1),
	.button2(button2),

	.led1(led1),
	.led2(led2)
	);
endmodule // soc_with_peripherals
