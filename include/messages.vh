`ifndef MESSAGES_VH
 `define MESSAGES_VH 1

 `define MSG(msg) \
     if (1) begin \
	$write("[%0t] ", $time); \
        $display msg; \
     end else

 `ifdef DEBUG
  `define DBG(msg) `MSG(msg)
 `else
  `define DBG(msg) if (1)  begin end else
 `endif

`endif

// Use like this: DBG(("Wishbone master: a = %x, b = %x", a, b))
