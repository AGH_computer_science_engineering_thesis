`define C(comment) // make comments that disapper after preprocessing

`C((
This file implements a kind of macro assembly to generate instructions for our
wishbone master mock module. The instructions specify what commands the master
should send to its wishbone interface and what intervals it should put in
between. $readmemh() is used to "load" the preprocessed instructions into
the simulation.

This header should be `included in the file with macroassembly operations.

To generate the actual memory file you can for example use the -E flag to
IVerilog, like this:
$ iverilog -E some_operations.memv -o some_operations.mem

I should have probably used some other tool for this job... POSIX shell, maybe?

operations:
  0 - read
  1 - write
  2 - wait (CYC_O high and STB_O low for one tick)
  3 - deselect (CYC_O low for one tick)
))

`define READ(addr, expected_data)        0 addr expected_data FFFF
`define READS(addr, expected_data, mask) 0 addr expected_data mask
`define WRITE(addr, data)                1 addr data FFFF
`define WRITES(addr, data, mask)         1 addr data mask
`define WAIT                             2 x x x
`define DESELECT                         3 x x x
